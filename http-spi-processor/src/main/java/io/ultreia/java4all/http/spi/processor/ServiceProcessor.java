package io.ultreia.java4all.http.spi.processor;

/*-
 * #%L
 * Http :: SPI Processor
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import io.ultreia.java4all.http.spi.Service;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import javax.tools.FileObject;
import javax.tools.JavaFileObject;
import javax.tools.StandardLocation;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

/**
 * Processes {@link Service} annotations and generates the service provider.
 * <p>
 * Processor Options:<ul>
 * <li>{@code -Adebug} - turns on debug statements</li>
 * <li>{@code -AHttpServicePackagePrefix=package} - translate relative method name for service in this package</li>
 * <li><b>Mandatory</b>{@code -AHttpProviderClassName=class} - provider fully qualified name</li>
 * <li><b>Mandatory</b>{@code -AHttpProviderSuperClassName=class} - provider super class fully qualified name</li>
 * </ul>
 * Created on 09/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.38
 */
@AutoService(Processor.class)
public class ServiceProcessor extends AbstractProcessor {
    public static final String GENERATE_PROVIDER_IMPL = "HttpGenerateProviderImpl";
    public static final String PROVIDER_CLASS_NAME = "HttpProviderClassName";
    public static final String PROVIDER_SUPER_CLASS_NAME = "HttpProviderSuperClassName";
    public static final String FACTORY_CLASS_NAME = "HttpFactoryClassName";
    public static final String SERVICE_CLASS_NAME = "HttpServiceClassName";
    public static final String CONFIGURATION_CLASS_NAME = "HttpConfigurationClassName";
    public static final String CONNECTION_CLASS_NAME = "HttpConnectionClassName";
    public static final String INIT_CONFIGURATION_CLASS_NAME = "HttpInitConfigurationClassName";
    public static final String INIT_CLASS_NAME = "HttpInitClassName";
    public static final String PACKAGE_PREFIX = "HttpServicePackagePrefix";
    public static final String TEMPLATE = "package %1$s;\n\n" +
            "import javax.annotation.Generated;\n\n" +
            "@Generated(value = \"io.ultreia.java4all.http.spi.processor.ServiceProcessor\", date = \"%2$s\")\n" +
            "public interface %3$s extends %4$s {\n\n" +
            "%5$s" +
            "}";

    public static final String TEMPLATE_IMPL = "package %1$s;\n\n" +
            "import io.ultreia.java4all.http.service.ServicesProviderSupport;\n\n" +
            "import javax.annotation.Generated;\n" +
            "import java.util.function.Supplier;\n\n" +
            "@Generated(value = \"io.ultreia.java4all.http.spi.processor.ServiceProcessor\", date = \"%2$s\")\n" +
            "public class %3$s extends ServicesProviderSupport<\n" +
            "        %4$s,\n" +
            "        %5$s,\n" +
            "        %6$s,\n" +
            "        %7$s,\n" +
            "        %8$s,\n" +
            "        %9$s> implements %10$s {\n" +
            "\n" +
            "    public %3$s(%9$s serviceFactory, Supplier<%8$s> serviceInitializer) {\n" +
            "        super(serviceFactory, serviceInitializer);\n" +
            "    }\n\n" +
            "    @Override\n" +
            "    public %8$s serviceInitializer(%8$s serviceInitializer, %6$s dataSourceConnection) {\n" +
            "        return new %8$s(serviceInitializer, null, dataSourceConnection);\n" +
            "    }\n\n" +
            "    @Override\n" +
            "    public %8$s serviceInitializer(%8$s serviceInitializer, %5$s dataSourceConfiguration) {\n" +
            "        return new %8$s(serviceInitializer, dataSourceConfiguration, null);\n" +
            "    }\n" +
            "}\n";
    public static final String METHOD = "    default %1$s %2$s() {\n" +
            "        return getService(%1$s.class);\n" +
            "    }\n";
    private final PackageServiceMethodNameTranslator translator = new PackageServiceMethodNameTranslator();
    private ProviderDef providerDefinition;

    static class ProviderDef {
        /**
         * Keys are type, values are method names.
         */
        private final Map<String, String> serviceMethods;
        private final String parentQualifiedName;
        private final String serviceClassName;
        private final String configurationClassName;
        private final String connectionClassName;
        private final String initConfigurationClassName;
        private final String initClassName;
        private final String factoryClassName;
        private final boolean generateImpl;
        private final String providerName;
        private final String providerPackageName;

        ProviderDef(String parentQualifiedName,
                    String providerQualifiedName,
                    String serviceClassName,
                    String configurationClassName,
                    String connectionClassName,
                    String initConfigurationClassName,
                    String initClassName,
                    String factoryClassName,
                    boolean generateImpl) {
            this.parentQualifiedName = parentQualifiedName;
            this.serviceClassName = serviceClassName;
            this.configurationClassName = configurationClassName;
            this.connectionClassName = connectionClassName;
            this.initConfigurationClassName = initConfigurationClassName;
            this.initClassName = initClassName;
            this.factoryClassName = factoryClassName;
            this.generateImpl = generateImpl;
            int endIndex = providerQualifiedName.lastIndexOf('.');
            this.providerPackageName = providerQualifiedName.substring(0, endIndex);
            this.providerName = providerQualifiedName.substring(endIndex + 1);
            this.serviceMethods = new TreeMap<>();
        }

        public boolean isGenerateImpl() {
            return generateImpl;
        }

        public String getServiceClassName() {
            return serviceClassName;
        }

        public String getConfigurationClassName() {
            return configurationClassName;
        }

        public String getConnectionClassName() {
            return connectionClassName;
        }

        public String getInitConfigurationClassName() {
            return initConfigurationClassName;
        }

        public String getInitClassName() {
            return initClassName;
        }

        public String getFactoryClassName() {
            return factoryClassName;
        }

        public String getProviderPackageName() {
            return providerPackageName;
        }

        public String getProviderQualifiedClassName() {
            return providerPackageName + "." + providerName;
        }

        public String getParentQualifiedName() {
            return parentQualifiedName;
        }

        public String getProviderName() {
            return providerName;
        }

        public void addService(String serviceQualifiedClassName, String methodName) {
            serviceMethods.put(serviceQualifiedClassName, methodName);
        }

        public Map<String, String> getServiceMethods() {
            return serviceMethods;
        }
    }

    @Override
    public Set<String> getSupportedOptions() {
        return Set.of("debug",
                      PACKAGE_PREFIX,
                      PROVIDER_CLASS_NAME,
                      PROVIDER_SUPER_CLASS_NAME,
                      SERVICE_CLASS_NAME,
                      CONFIGURATION_CLASS_NAME,
                      CONNECTION_CLASS_NAME,
                      INIT_CONFIGURATION_CLASS_NAME,
                      INIT_CLASS_NAME,
                      FACTORY_CLASS_NAME,
                      GENERATE_PROVIDER_IMPL);
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return Set.of(Service.class.getName());
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latest();
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

        if (providerDefinition == null) {
            String providerSuperClassName = loadOption(PROVIDER_SUPER_CLASS_NAME);
            String providerClassName = loadOption(PROVIDER_CLASS_NAME);
            String serviceClassName = loadOption(SERVICE_CLASS_NAME);
            String configurationClassName = loadOption(CONFIGURATION_CLASS_NAME);
            String connectionClassName = loadOption(CONNECTION_CLASS_NAME);
            String initConfigurationClassName = loadOption(INIT_CONFIGURATION_CLASS_NAME);
            String initClassName = loadOption(INIT_CLASS_NAME);
            String factoryClassName = loadOption(FACTORY_CLASS_NAME);
            String generateImpl = processingEnv.getOptions().get(GENERATE_PROVIDER_IMPL);

            providerDefinition = new ProviderDef(providerSuperClassName,
                                                 providerClassName,
                                                 serviceClassName,
                                                 configurationClassName,
                                                 connectionClassName,
                                                 initConfigurationClassName,
                                                 initClassName,
                                                 factoryClassName,
                                                 "true".equals(generateImpl));
        }

        if (roundEnv.processingOver()) {
            try {
                generateProvider();
            } catch (IOException e) {
                throw new RuntimeException(String.format("Can't generate ServicesProvider for: %s", providerDefinition.getProviderQualifiedClassName()), e);
            }
            return false;
        } else {
            String packagePrefix = processingEnv.getOptions().get(PACKAGE_PREFIX);
            translator.setPackageNamePrefix(packagePrefix != null ? packagePrefix : "$$");
            int beforeSize = providerDefinition.getServiceMethods().size();
            processAnnotations(annotations, roundEnv);
            int afterSize = providerDefinition.getServiceMethods().size();
            return afterSize > beforeSize;
        }
    }

    private String loadOption(String name) {
        return Objects.requireNonNull(processingEnv.getOptions().get(name), String.format("Need annotation option: %s", name));
    }

    private void processAnnotations(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        for (TypeElement annotation : annotations) {
            Set<? extends Element> annotatedElements = roundEnv.getElementsAnnotatedWith(annotation);
            for (Element annotatedElement : annotatedElements) {
                TypeElement classElement = (TypeElement) annotatedElement;
                logDebug(String.format("Detect Service: %s", classElement));
                String className = classElement.getQualifiedName().toString();
                String methodName = "get" + translator.getMethodName(className);
                providerDefinition.addService(className, methodName);
            }
        }
    }

    private void generateProvider() throws IOException {

        String providerPackageName = providerDefinition.getProviderPackageName();
        String providerName = providerDefinition.getProviderName();
        String providerParentName = providerDefinition.getParentQualifiedName();
        String className = providerDefinition.getProviderQualifiedClassName();
        try {
            FileObject resource = processingEnv.getFiler().getResource(StandardLocation.SOURCE_OUTPUT, providerPackageName, providerName + ".java");
            Path javaFile = Path.of(resource.toUri().toURL().getFile());
            if (Files.exists(javaFile)) {
                // Already done
                logWarning(String.format("Skip already processed class: %s", className));
                return;
            }
        } catch (IOException e) {
            // file not found, can safely execute it
        }
        Set<String> methods = new LinkedHashSet<>();
        for (Map.Entry<String, String> service : providerDefinition.serviceMethods.entrySet()) {
            methods.add(String.format(METHOD, service.getKey(), service.getValue()));
        }
        String content = String.format(TEMPLATE,
                                       providerPackageName,
                                       new Date(),
                                       providerName,
                                       providerParentName,
                                       String.join("\n", methods));

        logInfo(String.format("Generate services provider: %s", className));
        logDebug(String.format("Content:\n%s", content));
        generate(className, content);
        if (providerDefinition.isGenerateImpl()) {
            String classNameImpl = providerName + "Impl";
            String contentImpl = String.format(TEMPLATE_IMPL,
                                               providerPackageName,
                                               new Date(),
                                               classNameImpl,
                                               providerDefinition.getServiceClassName(),
                                               providerDefinition.getConfigurationClassName(),
                                               providerDefinition.getConnectionClassName(),
                                               providerDefinition.getInitConfigurationClassName(),
                                               providerDefinition.getInitClassName(),
                                               providerDefinition.getFactoryClassName(),
                                               providerName);

            logInfo(String.format("Generate services provider impl: %s", classNameImpl));
            logDebug(String.format("Content:\n%s", contentImpl));
            generate(providerPackageName + "." + classNameImpl, contentImpl);
        }
    }

    private void generate(String generatedClassName, String content) throws IOException {
        JavaFileObject builderFile = processingEnv.getFiler().createSourceFile(generatedClassName);
        try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {
            out.print(content);
        }
    }

    private void logInfo(String msg) {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, msg);
    }

    private void logDebug(String msg) {
        if (processingEnv.getOptions().containsKey("debug")) {
            processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, msg);
        }
    }

    private void logWarning(String msg) {
//        if (processingEnv.getOptions().containsKey("debug")) {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.WARNING, msg);
//        }
    }
}

