package io.ultreia.java4all.http.spi.processor;

/*-
 * #%L
 * Http :: SPI Processor
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.lang.Strings;

import java.util.Objects;
import java.util.Properties;

public class PackageServiceMethodNameTranslator {

    private String packageNamePrefix;

    public void setPackageNamePrefix(String packageNamePrefix) {
        this.packageNamePrefix = packageNamePrefix;
    }

    public void init(Properties properties) {
        packageNamePrefix = Objects.requireNonNull(properties.getProperty("http.service.package.prefix", "Can't find property http.service.package.prefix in maven project properties."));
    }

    public String getMethodName(String serviceType) {
        int endIndex = serviceType.lastIndexOf('.');
        String packageName = serviceType.substring(0, endIndex);

        if (packageName.startsWith(packageNamePrefix)) {
            return Strings.getRelativeCamelCaseName(packageNamePrefix, serviceType);
        }
        return serviceType.substring(endIndex + 1);
    }
}
