package io.ultreia.java4all.http;

/*-
 * #%L
 * Http :: Api
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * To serialize a response error.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class HResponseError {

    static final String PROPERTY_HTTP_CODE = "httpCode";

    static final String PROPERTY_EXCEPTION_TYPE = "exceptionType";

    static final String PROPERTY_MESSAGE = "message";

    static final String PROPERTY_EXCEPTION = "exception";

    private final Integer httpCode;

    private final Class<?> exceptionType;

    protected final String message;

    protected final Throwable exception;

    public HResponseError(Integer httpCode, Class<?> exceptionType, String message, Throwable exception) {
        this.httpCode = httpCode;
        this.exceptionType = exceptionType;
        this.message = message;
        this.exception = exception;
    }

    public Integer getHttpCode() {
        return httpCode;
    }

    public Class<?> getExceptionType() {
        return exceptionType;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getException() {
        return exception;
    }
}
