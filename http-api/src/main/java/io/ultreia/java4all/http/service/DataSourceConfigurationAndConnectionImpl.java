package io.ultreia.java4all.http.service;

/*-
 * #%L
 * Http :: Api
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public class DataSourceConfigurationAndConnectionImpl<Conf extends DataSourceConfiguration, Conn extends DataSourceConnection> implements DataSourceConfigurationAndConnection<Conf, Conn> {

    private Conf configuration;
    private Conn connection;

    public DataSourceConfigurationAndConnectionImpl(Conf configuration, Conn connection) {
        this.configuration = configuration;
        this.connection = connection;
    }

    @Override
    public Conf getConfiguration() {
        return configuration;
    }

    @Override
    public Conn getConnection() {
        return connection;
    }

    @Override
    public void setConfiguration(Conf configuration) {
        this.configuration = configuration;
    }

    @Override
    public void setConnection(Conn connection) {
        this.connection = connection;
    }

}
