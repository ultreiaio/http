package io.ultreia.java4all.http.service;

/*-
 * #%L
 * Http :: Api
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Objects;
import java.util.function.Supplier;

/**
 * Created on 08/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.38
 */
public abstract class ServicesProviderSupport<X extends HttpService,
        Conf extends DataSourceConfiguration,
        Conn extends DataSourceConnection,
        InitConf extends ServiceInitializerConfig,
        Init extends ServiceInitializer<InitConf, Conf, Conn>,
        F extends ServiceFactory<X, Conf, Conn, InitConf, Init>> implements ServicesProvider<X> {

    private final F serviceFactory;
    private final Supplier<Init> serviceInitializer;

    protected ServicesProviderSupport(F serviceFactory, Supplier<Init> serviceInitializer) {
        this.serviceFactory = Objects.requireNonNull(serviceFactory);
        this.serviceInitializer = Objects.requireNonNull(serviceInitializer);
    }

    @Override
    public <S extends X> S getService(Class<S> serviceType) {
        return serviceFactory.newService(serviceInitializer(), serviceType);
    }

    public final <S extends X> S newService(Conn dataSourceConnection, Class<S> serviceType) {
        Init initializer = serviceInitializer(serviceInitializer(), dataSourceConnection);
        return serviceFactory.newService(initializer, serviceType);
    }

    public final <S extends X> S newService(Conf dataSourceConfiguration, Class<S> serviceType) {
        Init initializer = serviceInitializer(serviceInitializer(), dataSourceConfiguration);
        return serviceFactory.newService(initializer, serviceType);
    }

    public abstract Init serviceInitializer(Init init, Conn dataSourceConnection);

    public abstract Init serviceInitializer(Init init, Conf dataSourceConfiguration);


    public F serviceFactory() {
        return serviceFactory;
    }

    public Init serviceInitializer() {
        return serviceInitializer.get();
    }
}
