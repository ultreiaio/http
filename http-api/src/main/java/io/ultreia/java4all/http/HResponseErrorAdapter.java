package io.ultreia.java4all.http;

/*-
 * #%L
 * Http :: Api
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public class HResponseErrorAdapter implements JsonDeserializer<HResponseError> {
    @Override
    public HResponseError deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        Integer httpCode = context.deserialize(jsonObject.get(HResponseError.PROPERTY_HTTP_CODE), Integer.class);
        String message = context.deserialize(jsonObject.get(HResponseError.PROPERTY_MESSAGE), String.class);
        Class<?> exceptionType = context.deserialize(jsonObject.get(HResponseError.PROPERTY_EXCEPTION_TYPE), Class.class);
        Throwable exception = null;
        if (exceptionType != null) {
            JsonObject exceptionJsonObject = jsonObject.get(HResponseError.PROPERTY_EXCEPTION).getAsJsonObject();
            try {
                exception = context.deserialize(exceptionJsonObject, exceptionType);
            } catch (JsonParseException e) {
                exception = onJsonParseException(exceptionType, context,exceptionJsonObject, message, e);
            }
        }
        return new HResponseError(httpCode, exceptionType, message, exception);
    }

    protected Throwable onJsonParseException(Class<?> exceptionType, JsonDeserializationContext context, JsonObject exceptionJsonObject, String message, JsonParseException jsonException ) {
        return new RuntimeException(String.format("Get error of type: %s (message: %s), but could not deserialize it.", exceptionType, message), jsonException);
    }
}
