package io.ultreia.java4all.http;

/*-
 * #%L
 * Http :: Api
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * Created by tchemit on 01/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class HttpResponseBadStatusCodeException extends RuntimeException {

    private final int expectedCode;
    private final int responseCode;

    public HttpResponseBadStatusCodeException(int expectedCode, int responseCode) {
        this.expectedCode = expectedCode;
        this.responseCode = responseCode;
    }

    public int getExpectedCode() {
        return expectedCode;
    }

    public int getResponseCode() {
        return responseCode;
    }
}
