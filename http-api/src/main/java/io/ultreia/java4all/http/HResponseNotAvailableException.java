package io.ultreia.java4all.http;

/*
 * #%L
 * Http :: Api
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.net.URI;

/**
 * Exception à retourner quand le service n'est pas accessible.
 * <p>
 * Created on 06/09/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class HResponseNotAvailableException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private final URI serverUrl;
    private final int errorCode;

    HResponseNotAvailableException(URI serverUrl, int errorCode) {
        super(String.format("Server %s not available, http error code: %d", serverUrl, errorCode));
        this.serverUrl = serverUrl;
        this.errorCode = errorCode;
    }

    public URI getServerUrl() {
        return serverUrl;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
