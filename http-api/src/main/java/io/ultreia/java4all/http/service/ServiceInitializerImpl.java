package io.ultreia.java4all.http.service;

/*
 * #%L
 * Http :: Api
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


import java.util.Objects;

/**
 * Objet contentant les informations nécessaire pour créer un nouveau service.
 * <p>
 * Ces informations viennent en général du context applicatif appelant.
 * <p>
 * Created on 31/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ServiceInitializerImpl<C extends ServiceInitializerConfig, Conf extends DataSourceConfiguration, Conn extends DataSourceConnection> implements DataSourceConfigurationAndConnection<Conf, Conn>, ServiceInitializer<C, Conf, Conn> {

    private final C initializerConfig;
    private final DataSourceConfigurationAndConnection<Conf, Conn> configurationAndConnection;

    public ServiceInitializerImpl(ServiceInitializer<C, Conf, Conn> initializer, Conf Conf, Conn Conn) {
        this(initializer.getInitializerConfig(), Conf, Conn);
    }

    public ServiceInitializerImpl(C initializerConfig, Conf Conf, Conn Conn) {
        this.initializerConfig = Objects.requireNonNull(initializerConfig);
        this.configurationAndConnection = new DataSourceConfigurationAndConnectionImpl<>(Conf, Conn);
    }

    public C getInitializerConfig() {
        return initializerConfig;
    }

    @Override
    public Conf getConfiguration() {
        return configurationAndConnection.getConfiguration();
    }

    @Override
    public Conn getConnection() {
        return configurationAndConnection.getConnection();
    }

    @Override
    public void setConfiguration(Conf configuration) {
        this.configurationAndConnection.setConfiguration(configuration);
    }

    @Override
    public void setConnection(Conn connection) {
        this.configurationAndConnection.setConnection(connection);
    }
}
