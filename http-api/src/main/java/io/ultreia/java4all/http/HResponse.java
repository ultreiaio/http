package io.ultreia.java4all.http;

/*-
 * #%L
 * Http :: Api
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.hc.core5.http.Header;

import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Created by tchemit on 14/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@SuppressWarnings("unused")
public class HResponse {

    private final Integer statusCode;

    private final String responseAsString;

    private final Gson gson;
    private final Header[] responseHeaders;

    HResponse(Integer statusCode, String responseAsString, Gson gson, Header... responseHeaders) {
        this.statusCode = statusCode;
        this.responseAsString = responseAsString;
        this.gson = gson;
        this.responseHeaders = responseHeaders;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    @Override
    public String toString() {
        return responseAsString;
    }

    public Header[] getResponseHeaders() {
        return responseHeaders;
    }

    public int toInt() {
        return Integer.parseInt(responseAsString.trim().replaceAll("\"", ""));
    }

    public long toLong() {
        return Long.parseLong(responseAsString.trim().replaceAll("\"", ""));
    }

    public boolean toBoolean() {
        return Boolean.parseBoolean(responseAsString.trim().replaceAll("\"", ""));
    }

    public float toFloat() {
        return Float.parseFloat(responseAsString.trim().replaceAll("\"", ""));
    }

    public double toDouble() {
        return Double.parseDouble(responseAsString.trim().replaceAll("\"", ""));
    }

    public Map<String, Object> toJson() {
        return toJson(TypeToken.getParameterized(LinkedHashMap.class, String.class, Object.class).getType());
    }

    public <T> T toJson(Type type) {
        return gson.fromJson(responseAsString, type);
    }

    public <O> Optional<O> toOptional(Class<O> type) {
        return toJson(token(Optional.class, type));
    }

    public <O> Set<O> toSet(Class<O> type) {
        return toJson(token(Set.class, type));
    }

    public <O> List<O> toList(Class<O> type) {
        return toJson(token(List.class, type));
    }

    private Type token(Class<?> main, Class<?> type) {
        return TypeToken.getParameterized(main, type).getType();
    }
}
