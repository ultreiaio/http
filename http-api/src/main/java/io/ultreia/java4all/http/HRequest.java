package io.ultreia.java4all.http;

/*
 * #%L
 * Http :: Api
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.http.spi.RequestMethod;

import java.io.File;
import java.util.Collections;
import java.util.Map;

/**
 * Created on 06/09/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class HRequest {

    private final RequestMethod requestMethod;

    private final String baseUrl;

    private final String contentType;

    private final String requestBody;

    private final Map<String, String> headers;

    private final Map<String,String> parameters;

    private final Map<String, File> files;

    private final long timeout;
    private final boolean useMultipartForm;

    HRequest(RequestMethod requestMethod,
             String baseUrl,
             String requestBody,
             String contentType,
             Map<String, String> header,
             Map<String,String> params,
             Map<String, File> files,
             long timeout,
             boolean useMultipartForm) {
        this.requestMethod = requestMethod;
        this.baseUrl = baseUrl;
        this.requestBody = requestBody;
        this.contentType = contentType;
        this.headers = Collections.unmodifiableMap(header);
        this.parameters = Collections.unmodifiableMap(params);
        this.files = Collections.unmodifiableMap(files);
        this.timeout = timeout;
        this.useMultipartForm = useMultipartForm;
    }

    public RequestMethod getRequestMethod() {
        return requestMethod;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public String getContentType() {
        return contentType;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public Map<String, File> getFiles() {
        return files;
    }

    public boolean isUseMultipartForm() {
        return useMultipartForm;
    }

    public long getTimeout() {
        return timeout;
    }

    public boolean withFiles() {
        return !files.isEmpty();
    }

    @Override
    public String toString() {
        return String.format("[%S] %s", getRequestMethod(), getBaseUrl());
    }
}
