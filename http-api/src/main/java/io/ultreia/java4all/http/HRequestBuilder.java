package io.ultreia.java4all.http;

/*
 * #%L
 * Http :: Api
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.http.spi.RequestMethod;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
@SuppressWarnings({"unused", "UnusedReturnValue"})
public class HRequestBuilder {

    @SuppressWarnings("SpellCheckingInspection")
    public static final String DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ssZ";

    protected final Map<String, File> files;
    private final Map<String, String> parameters;
    private final Map<String, String> headers;
    private final String baseUrl;
    protected Supplier<String> authenticationTokenSupplier;
    private String requestBody;
    private String contentType;
    private RequestMethod requestMethod;
    private long timeout = 0;
    private boolean addAuthenticationToken;
    private boolean useMultipartForm;
    private SimpleDateFormat dateFormat;

    public HRequestBuilder(String baseUrl) {
        Objects.requireNonNull(baseUrl);
        this.baseUrl = baseUrl;
        this.headers = new LinkedHashMap<>();
        this.files = new LinkedHashMap<>();
        this.parameters = new LinkedHashMap<>();
    }

    public HRequest delete() {
        this.requestMethod = RequestMethod.DELETE;
        return build();
    }

    public HRequest get() {
        this.requestMethod = RequestMethod.GET;
        return build();
    }

    public HRequest post() {
        this.requestMethod = RequestMethod.POST;
        return build();
    }

    public HRequest put() {
        this.requestMethod = RequestMethod.PUT;
        return build();
    }

    protected HRequest build() {

        checkBeforeBuild();

        if (addAuthenticationToken) {
            setAuthenticationTokenInRequest();
        }

        return new HRequest(
                requestMethod,
                baseUrl,
                requestBody,
                contentType,
                headers,
                parameters,
                files,
                timeout,
                useMultipartForm
        );
    }

    protected void checkBeforeBuild() {
        Objects.requireNonNull(baseUrl, "'baseUrl' can't be null");
        Objects.requireNonNull(requestMethod, "'requestMethod' was not set");

        if (addAuthenticationToken) {
            Objects.requireNonNull(authenticationTokenSupplier.get(), "No auth token provided but request need it.");
        }
    }

    protected void setAuthenticationTokenInRequest() {
        addHeader("Authorization", authenticationTokenSupplier.get());
    }

    public HRequestBuilder addAuthenticationTokenSupplier(Supplier<String> authTokenSupplier) {
        this.authenticationTokenSupplier = authTokenSupplier;
        return this;
    }

    public HRequestBuilder addAuthenticationToken() {
        this.addAuthenticationToken = true;
        return this;
    }

    public HRequestBuilder useMultiPartForm() {
        this.useMultipartForm = true;
        return this;
    }

    public HRequestBuilder setTimeout(TimeUnit timeUnit, int timeout) {
        this.timeout = Objects.requireNonNull(timeUnit).toMillis(timeout);
        return this;
    }

    public HRequestBuilder addHeader(String key, String value) {
        checkRequestNotNull(key, value);
        headers.put(key, value);
        return this;
    }

    public HRequestBuilder addParameter(String parameterName, String value) {
        if (value != null) {
            parameters.put(parameterName, value);
        }
        return this;
    }

    public HRequestBuilder addParameter(String parameterName, Class<?> value) {
        if (value != null) {
            parameters.put(parameterName, value.getName());
        }
        return this;
    }

    public HRequestBuilder addParameter(String parameterName, Date value) {
        if (value != null) {
            parameters.put(parameterName, getDateFormat().format(value));
        }
        return this;
    }

    public HRequestBuilder addParameter(String parameterName, Number value) {
        if (value != null) {
            parameters.put(parameterName, String.valueOf(value));
        }
        return this;
    }

    public HRequestBuilder addParameter(String parameterName, Boolean value) {
        if (value != null) {
            parameters.put(parameterName, String.valueOf(value));
        }
        return this;
    }

    public HRequestBuilder addFile(String parameterName, File file) {
        if (file != null) {
            files.put(parameterName, file);
        }
        return this;
    }

    public HRequestBuilder addParameter(String parameterName, int value) {
        parameters.put(parameterName, String.valueOf(value));
        return this;
    }

    public HRequestBuilder addParameter(String parameterName, long value) {
        parameters.put(parameterName, String.valueOf(value));
        return this;
    }

    public HRequestBuilder addParameter(String parameterName, float value) {
        parameters.put(parameterName, String.valueOf(value));
        return this;
    }

    public HRequestBuilder addParameter(String parameterName, double value) {
        parameters.put(parameterName, String.valueOf(value));
        return this;
    }

    public HRequestBuilder addParameter(String parameterName, boolean value) {
        parameters.put(parameterName, String.valueOf(value));
        return this;
    }

    public HRequestBuilder setRequestBody(String requestBody) {
        this.requestBody = requestBody;
        return this;
    }

    public HRequestBuilder addHeaders(Map<String, String> headers) {
        for (Map.Entry<String, String> entry : headers.entrySet()) {
            addHeader(entry.getKey(), entry.getValue());
        }
        return this;
    }

    public HRequestBuilder setContentType(String contentType) {
        this.contentType = contentType;
        return this;
    }

    protected SimpleDateFormat getDateFormat() {
        if (dateFormat == null) {
            dateFormat = new SimpleDateFormat(DATE_PATTERN);
        }
        return dateFormat;
    }

    protected void checkRequestNotNull(String key, Object value) {
        Objects.requireNonNull(key, "Request key must be not null");
        Objects.requireNonNull(value, "Request value must be not null for key : " + key);
    }
}
