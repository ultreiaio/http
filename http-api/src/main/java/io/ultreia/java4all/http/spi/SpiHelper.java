package io.ultreia.java4all.http.spi;

/*-
 * #%L
 * Http :: Api
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Objects;

/**
 * Created by tchemit on 17/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class SpiHelper {

    public static RequestAnnotation getRequestAnnotation(Method method) {
        {
            Get annotation = method.getAnnotation(Get.class);
            if (annotation != null) {
                return new RequestAnnotation(Get.class.getSimpleName(),
                                             false,
                                             annotation.timeOut());
            }
        }
        {
            Post annotation = method.getAnnotation(Post.class);
            if (annotation != null) {
                return new RequestAnnotation(Post.class.getSimpleName(),
                                             annotation.useMultiPartForm(),
                                             annotation.timeOut());
            }
        }
        {
            Delete annotation = method.getAnnotation(Delete.class);
            if (annotation != null) {
                return new RequestAnnotation(Delete.class.getSimpleName(),
                                             annotation.useMultiPartForm(),
                                             annotation.timeOut());
            }
        }
        {
            Put annotation = method.getAnnotation(Put.class);
            if (annotation != null) {
                return new RequestAnnotation(Put.class.getSimpleName(),
                                             annotation.useMultiPartForm(),
                                             annotation.timeOut());
            }
        }

        throw new IllegalStateException(String.format("Could not find request annotation on method: %s.%s", method.getDeclaringClass().getName(), method.getName()));
    }

    public static boolean anonymous(Class<?> serviceType) {
        Service annotation = serviceType.getAnnotation(Service.class);
        return Objects.requireNonNull(annotation, "Can't find annotation Service on class " + serviceType.getName()).anonymous();
    }

    public static boolean write(Method method) {
        return method.isAnnotationPresent(Write.class);
    }

    public static boolean nullable(Parameter parameter) {
        return parameter.isAnnotationPresent(Nullable.class);
    }

}
