package io.ultreia.java4all.http.service;

/*
 * #%L
 * Http :: Api
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created on 16/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class ServiceMainFactorySupport<
        X extends HttpService,
        Conf extends DataSourceConfiguration,
        Conn extends DataSourceConnection,
        InitConf extends ServiceInitializerConfig,
        Init extends ServiceInitializer<InitConf, Conf, Conn>,
        F extends ServiceFactory<X, Conf, Conn, InitConf, Init>> implements ServiceFactory<X, Conf, Conn, InitConf, Init> {

    private static final Logger log = LogManager.getLogger(ServiceMainFactorySupport.class);
    private static ExecutorService EXECUTOR_SERVICE;
    private final Set<F> delegateFactories;

    public static ExecutorService getExecutorService() {
        if (EXECUTOR_SERVICE == null) {
            EXECUTOR_SERVICE = Executors.newFixedThreadPool(5);
            log.info(String.format("Create executor service: %s", EXECUTOR_SERVICE));
        }
        return EXECUTOR_SERVICE;
    }

    protected ServiceMainFactorySupport(Class<F> type) {
        log.info(String.format("Service factory initialization  (%s).", this));
        Set<F> builder = new LinkedHashSet<>();
        for (F factory : ServiceLoader.load(Objects.requireNonNull(type, "Factory type can't be null."))) {
            log.info(String.format("Discover service factory: %s", factory));
//            factory.setMainServiceFactory(type.cast(this));
            builder.add(factory);
        }
        if (builder.isEmpty()) {
            throw new IllegalStateException("No service factory found.");
        }
        log.info(String.format("Discover %d service factories.", builder.size()));
        this.delegateFactories = Collections.unmodifiableSet(builder);
        log.info(String.format("Service factory is initialized  (%s).", this));
    }

    @Override
    public <S extends X> boolean accept(Conf dataSourceConfiguration, Class<S> serviceType) {
        // main factory accept any service
        return true;
    }

    @Override
    public <S extends X> boolean accept(Conn dataSourceConnection, Class<S> serviceType) {
        // main factory accept any service
        return true;
    }

    @Override
    public <S extends X> S newService(Init serviceInitializer, Class<S> serviceType) {
        Objects.requireNonNull(serviceInitializer, "serviceInitializerContext can't be null.");
        Objects.requireNonNull(serviceType, "serviceType can't be null.");
        F factory;
        if (serviceInitializer.withConnection()) {
            factory = getFactory(serviceInitializer.getConnection(), serviceType);
        } else if (serviceInitializer.withConfiguration()) {
            factory = getFactory(serviceInitializer.getConfiguration(), serviceType);
        } else {
            throw new IllegalStateException("No dataSourceConnection, nor dataSourceConfiguration given.");
        }
        Objects.requireNonNull(factory, "factory can't be null.");
        log.trace(String.format("Using factory: %s", factory));
        S service = factory.newService(serviceInitializer, serviceType);
        log.debug(String.format("New service created: %s", service));
        return service;
    }

    @Override
    public void close() {
        for (F delegateFactory : delegateFactories) {
            try {
                delegateFactory.close();
            } catch (Exception e) {
                log.error(String.format("Could not close factory: %s", delegateFactory), e);
            }
        }
        if (EXECUTOR_SERVICE != null) {
            log.info(String.format("Close executor service: %s", EXECUTOR_SERVICE));
            try {
                EXECUTOR_SERVICE.shutdown();
            } finally {
                EXECUTOR_SERVICE = null;
            }
        }
    }

    private <S extends X> F getFactory(Conf dataSourceConfiguration, Class<S> serviceType) {
        Objects.requireNonNull(dataSourceConfiguration);
        Objects.requireNonNull(serviceType);
        return delegateFactories.stream()
                .filter(f -> f.accept(dataSourceConfiguration, serviceType))
                .findFirst().orElseThrow(() -> new IllegalStateException(String.format("No factory found for dataSourceConfiguration: %s and serviceType: %s", dataSourceConfiguration, serviceType.getName())));
    }

    private <S extends X> F getFactory(Conn dataSourceConnection, Class<S> serviceType) {
        Objects.requireNonNull(dataSourceConnection);
        Objects.requireNonNull(serviceType);
        return delegateFactories.stream()
                .filter(f -> f.accept(dataSourceConnection, serviceType))
                .findFirst().orElseThrow(() -> new IllegalStateException(String.format("No factory found for dataSourceConnection: %s and serviceType: %s", dataSourceConnection, serviceType.getName())));
    }

}
