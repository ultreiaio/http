package io.ultreia.java4all.http;

/*
 * #%L
 * Http :: Api
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.ultreia.java4all.http.spi.RequestMethod;
import org.apache.hc.client5.http.classic.methods.HttpDelete;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.classic.methods.HttpPut;
import org.apache.hc.client5.http.classic.methods.HttpUriRequestBase;
import org.apache.hc.client5.http.config.RequestConfig;
import org.apache.hc.client5.http.cookie.BasicCookieStore;
import org.apache.hc.client5.http.entity.UrlEncodedFormEntity;
import org.apache.hc.client5.http.entity.mime.MultipartEntityBuilder;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClientBuilder;
import org.apache.hc.core5.http.ClassicHttpResponse;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.Header;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.NameValuePair;
import org.apache.hc.core5.http.io.entity.StringEntity;
import org.apache.hc.core5.http.message.BasicNameValuePair;
import org.apache.hc.core5.net.URIBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public class HResponseBuilder implements Closeable {

    private static final Logger log = LogManager.getLogger(HResponseBuilder.class);
    private final CloseableHttpClient client;
    private final Supplier<Gson> gson;

    public static HResponseBuilder create() {
        return create(null);
    }

    public static HResponseBuilder create(Supplier<Gson> gson, CloseableHttpClient build) {
        return new HResponseBuilder(build, gson);
    }

    public static HResponseBuilder create(Supplier<Gson> gson) {
        return create(gson, HttpClientBuilder.create()
                                             .setDefaultCookieStore(new BasicCookieStore())
//                                             .setDefaultRequestConfig(RequestConfig.custom().setResponseTimeout(1, TimeUnit.MINUTES).build())
                                             //FIXME-migration
//                .setMaxConnTotal(1000)
//                                             .setMaxConnPerRoute(1000)
//                .setConnectionTimeToLive(45, TimeUnit.SECONDS)
                                             .build());
    }

    private HResponseBuilder(CloseableHttpClient client, Supplier<Gson> gson) {
        this.client = client;
        this.gson = gson == null ? () -> new GsonBuilder().setPrettyPrinting().create() : gson;
    }

    public Supplier<Gson> getGson() {
        return gson;
    }

    public HResponse executeRequest(HRequest request, int expectedStatusCode) {
        HResponse response = executeRequest(request);

        Integer statusCode = response.getStatusCode();
        if (Objects.equals(statusCode, expectedStatusCode)) {
            return response;
        }

        String baseUrl = request.getBaseUrl();
        String responseAsString = response.toString();
        log.warn(String.format("Unexpected status code for url: %s\n%s", baseUrl, responseAsString));

        if (statusCode >= 400 && statusCode < 500) {
            throw new HResponseNotAvailableException(URI.create(baseUrl), statusCode);
        }

        if (statusCode >= 500) {
            HResponseError error;
            try {
                error = getGson().get().fromJson(responseAsString, HResponseError.class);
            } catch (Exception e) {
                error = new HResponseError(statusCode, null, responseAsString, null);
            }
            throw new HResponseErrorException(error);
        }

        throw new HttpResponseBadStatusCodeException(expectedStatusCode, statusCode);
    }

    public HResponse executeRequest(HRequest request) {

        if (log.isInfoEnabled()) {
            log.info(String.format("execute request: [%s] %s", request.getRequestMethod(), request.getBaseUrl()));
        }
        log.debug("request params: " + request.getParameters());

        RequestMethod requestMethod = request.getRequestMethod();

        HttpUriRequestBase httpRequest;
        switch (requestMethod) {
            case GET:
                httpRequest = fillRequestWithoutBody(request, new HttpGet(buildUrlWithParameters(request.getBaseUrl(), request.getParameters())));
                break;
            case DELETE:
                httpRequest = fillRequestWithoutBody(request, new HttpDelete(buildUrlWithParameters(request.getBaseUrl(), request.getParameters())));
                break;
            case PUT:
                httpRequest = fillRequestWithBody(request, new HttpPut(request.getBaseUrl()));
                break;
            case POST:
                httpRequest = fillRequestWithBody(request, new HttpPost(request.getBaseUrl()));
                break;
            default:
                throw new IllegalStateException(String.format("This request [%s] method is not yet implemented.", requestMethod));
        }
        HResponse response = executeRequestAndConsume(request, httpRequest);

        if (log.isDebugEnabled()) {
            log.debug("status: " + response.getStatusCode());
            for (Header header : response.getResponseHeaders()) {
                log.debug("response header: " + header);
            }
        }
        return response;
    }

    @Override
    public void close() throws IOException {
        client.close();
    }

    private <M extends HttpUriRequestBase> M fillRequestWithoutBody(HRequest request, M method) {
        addTimeoutToRequest(method, request.getTimeout());
        addHeaders(method, request.getHeaders());
        return method;
    }

    private <M extends HttpUriRequestBase> M fillRequestWithBody(HRequest request, M method) {
        String requestBody = request.getRequestBody();
        addTimeoutToRequest(method, request.getTimeout());
        addHeaders(method, request.getHeaders());
        ContentType type = getContentType(request);
        addRequestBody(method, type, requestBody);
        if (method.getEntity() == null) {
            HttpEntity entity;
            if (request.isUseMultipartForm()) {
                MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
                entityBuilder.setContentType(ContentType.MULTIPART_FORM_DATA.withCharset(StandardCharsets.UTF_8));
                entityBuilder.setCharset(StandardCharsets.UTF_8);
                if (request.withFiles()) {
                    try {
                        for (Map.Entry<String, File> paramFile : request.getFiles().entrySet()) {
                            byte[] content = Files.readAllBytes(paramFile.getValue().toPath());
                            ContentType contentType1 = ContentType.create(Files.probeContentType(paramFile.getValue().toPath()));
                            entityBuilder.addBinaryBody(paramFile.getKey(), content, contentType1, paramFile.getValue().getName());
                        }
                    } catch (IOException e) {
                        throw new IllegalStateException("Io error while preparing request...", e);
                    }
                }
                for (Map.Entry<String, String> param : request.getParameters().entrySet()) {
                    entityBuilder.addTextBody(param.getKey(), param.getValue(), type);
                }
                entity = entityBuilder.build();
            } else {
                entity = new UrlEncodedFormEntity(toNameValuePairList(request.getParameters()), StandardCharsets.UTF_8);
            }
            method.setEntity(entity);
        }
        return method;
    }

    private ContentType getContentType(HRequest request) {
        String contentType = request.getContentType();
        if (contentType == null || contentType.trim().isEmpty()) {
            return ContentType.TEXT_PLAIN.withCharset(StandardCharsets.UTF_8);
        }
        return ContentType.create(contentType, StandardCharsets.UTF_8);
    }

    private HResponse executeRequestAndConsume(HRequest request, HttpUriRequestBase httpUriRequestBase) throws HResponseNotAvailableException {
        try {
            return client.execute(httpUriRequestBase, response -> {
                String baseUrl = request.getBaseUrl();
                log.debug("{} '{} return status code : {} ", httpUriRequestBase.getMethod(), baseUrl, response.getCode());
                return consumeResponse(request, response);
            });
        } catch (UnknownHostException | ConnectException e) {
            try {
                throw new HResponseNotAvailableException(httpUriRequestBase.getUri(), -1);
            } catch (URISyntaxException ex) {
                throw new RuntimeException(ex);
            }
        } catch (IOException e) {
            throw new IllegalStateException("Io error while executing request...", e);
        }
    }

    private HResponse consumeResponse(HRequest request, ClassicHttpResponse response) {
        String baseUrl = request.getBaseUrl();
        Header[] responseHeaders = response.getHeaders();
        int statusCode = response.getCode();
        if (log.isDebugEnabled()) {
            log.debug(request.getRequestMethod() + " '" + baseUrl + "' return status code : " + statusCode);
        }
        String responseAsString;
        try (InputStream inputStream = response.getEntity().getContent()) {
            responseAsString = new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new IllegalStateException("IO exception while consuming response", e);
        }
        return new HResponse(statusCode, responseAsString, gson.get(), responseHeaders);
    }

    private String buildUrlWithParameters(String baseUrl, Map<String, String> parameters) {
        if (parameters.isEmpty()) {
            return baseUrl;
        }
        try {
            return new URIBuilder(baseUrl, StandardCharsets.UTF_8).setParameters(toNameValuePairList(parameters)).build().toString();
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    private <M extends HttpUriRequestBase> void addHeaders(M httpMethod, Map<String, String> headers) {
        for (Map.Entry<String, String> entry : headers.entrySet()) {
            httpMethod.setHeader(entry.getKey(), entry.getValue());
        }
    }

    private <M extends HttpUriRequestBase> void addRequestBody(M method, ContentType contentType, String requestBody) {
        if (requestBody != null && !requestBody.trim().isEmpty()) {
            method.setEntity(new StringEntity(requestBody, contentType));
        }
    }

    private List<NameValuePair> toNameValuePairList(Map<String, String> parameters) {
        return parameters.entrySet().stream().map(e -> new BasicNameValuePair(e.getKey(), e.getValue())).collect(Collectors.toList());
    }

    private void addTimeoutToRequest(HttpUriRequestBase httpRequestBase, long timeout) {
        if (timeout > 0) {
            RequestConfig requestConfig = RequestConfig
                    .custom()
                    .setResponseTimeout(timeout, TimeUnit.MILLISECONDS).build();
            httpRequestBase.setConfig(requestConfig);
        }
    }

}
