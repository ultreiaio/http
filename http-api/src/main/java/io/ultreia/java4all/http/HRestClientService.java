package io.ultreia.java4all.http;

/*-
 * #%L
 * Http :: Api
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by tchemit on 13/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface HRestClientService {

    String PARAMETERIZED_TYPE_PREFIX = "parameterized_type_";

    default Gson gson() {
        return new GsonBuilder().setPrettyPrinting().create();
    }

    HRequestBuilderFactory getRequestBuilderFactory();

    default HRequestBuilder create(String baseUrl) {
        return getRequestBuilderFactory().create(baseUrl);
    }

    HResponse executeRequest(HRequest request);

    HResponse executeRequest(HRequest request, int expectedStatusCode);

}
