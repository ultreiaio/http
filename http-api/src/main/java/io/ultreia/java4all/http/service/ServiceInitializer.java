package io.ultreia.java4all.http.service;

/*-
 * #%L
 * Http :: Api
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * To initialize a service.
 * <p>
 * Created on 08/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.38
 */
public interface ServiceInitializer<InitConf extends ServiceInitializerConfig, Conf extends DataSourceConfiguration, Conn extends DataSourceConnection> extends DataSourceConfigurationAndConnection<Conf, Conn> {

    InitConf getInitializerConfig();
}
