package io.ultreia.java4all.http.maven.plugin.test;

/*-
 * #%L
 * Http :: Maven Plugin
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.github.mustachejava.Mustache;
import io.ultreia.java4all.http.maven.plugin.HttpMojoSupport;
import io.ultreia.java4all.http.spi.model.ClassDescription;
import io.ultreia.java4all.http.spi.model.ImportManager;
import io.ultreia.java4all.http.spi.model.MethodDescription;
import io.ultreia.java4all.http.spi.model.MethodDescriptionImpl;
import io.ultreia.java4all.http.spi.model.ServiceMapping;
import io.ultreia.java4all.lang.Strings;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.junit.Assert;
import org.junit.Assume;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 01/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.26
 */
@Mojo(name = "generate-api-test-fixtures", threadSafe = true, defaultPhase = LifecyclePhase.GENERATE_SOURCES, requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME)
public class GenerateApiTestFixturesMojo extends HttpMojoSupport {

    /**
     * Where to generate classes.
     */
    @Parameter(defaultValue = "${project.build.directory}/generated-sources/http", required = true)
    private File outputDirectory;

    /**
     * Where are the sources (used to detect if we should generate concrete classes).
     */
    @Parameter(defaultValue = "${project.basedir}/src/main/java", required = true)
    private File sourcesDirectory;

    /**
     * Where are the resources.
     */
    @Parameter(defaultValue = "${project.basedir}/src/main/resources", required = true)
    private File resourcesDirectory;

    /**
     * Suffix to add to generated classes name.
     */
    @Parameter(required = true, defaultValue = "Fixtures")
    private String classSuffix;

    /**
     * Service support.
     */
    @Parameter(required = true)
    private String serviceSupportType;

    /**
     * Package name where to generate.
     */
    @Parameter(required = true)
    private String packageName;
    /**
     * Optional service provider name to use in generated files.
     */
    @Parameter(property = "http.serviceProviderName")
    private String serviceProviderName;
    /**
     * To generate missing concrete files in sources.
     */
    @Parameter(defaultValue = "false")
    private boolean generateConcreteToSource;

    private String serviceSupportTypeSimpleName;

    private static class FixturesProperties {
        private final Properties properties;
        private final Set<String> defaultProperties;

        private FixturesProperties() {
            this.properties = new Properties();
            this.defaultProperties = new LinkedHashSet<>();
        }

        public void register(String methodPrefix, java.lang.reflect.Parameter parameter) {
            if (parameter.getType() == String.class) {
                String propertyName = methodPrefix + parameter.getName();
                String defaultProperty = "default" + Strings.capitalize(parameter.getName());
                if (properties.getProperty(propertyName) == null) {
                    properties.setProperty(propertyName, "${" + defaultProperty + "}");
                }
                defaultProperties.add(defaultProperty);
            }
        }

        public void flush(Path path) {
            for (String propertyName : defaultProperties) {
                if (properties.getProperty(propertyName) == null) {
                    properties.setProperty(propertyName, "FIXME");
                }
            }

            List<String> names = new ArrayList<>(properties.stringPropertyNames());
            names.sort(String::compareTo);
            List<String> lines = names.stream().map(k -> String.format("%s=%s", k, properties.getProperty(k))).collect(Collectors.toList());
            try {
                if (Files.notExists(path.getParent())) {
                    Files.createDirectories(path.getParent());
                }
                Files.write(path, lines);
            } catch (IOException e) {
                throw new IllegalStateException("Can't write  fixtures properties at: " + path, e);
            }
        }

        public void load(Path resourceExistPath) {
            if (Files.exists(resourceExistPath)) {
                try (BufferedReader reader = Files.newBufferedReader(resourceExistPath)) {
                    properties.load(reader);
                } catch (IOException e) {
                    throw new IllegalStateException("Can't read existing fixtures properties at: " + resourceExistPath, e);
                }
            }
        }
    }

    @Override
    protected void doAction() throws MojoExecutionException {

        Class<?> serviceTypeClass = getClass(serviceType);

        serviceSupportTypeSimpleName = serviceSupportType.substring(serviceSupportType.lastIndexOf(".") + 1);

        Path targetPath = getTargetPath(outputDirectory, packageName);
        Path sourcePath = getTargetPath(sourcesDirectory, packageName);
        Path resourcePath = getTargetPath(resourcesDirectory.toPath().resolve("fixtures").toFile(), packageName);

        getLog().debug(String.format("Will generate in: %s", targetPath));

        project.addCompileSourceRoot(outputDirectory.toString());

        Mustache mustache = getMustache(getClass(), "Generated");
        Mustache mustacheConcrete = getMustache(getClass(), "Concrete");

        List<Class<?>> services = getServices(serviceTypeClass);

        String servicePackageName = serviceTypeClass.getPackage().getName();

        for (Class<?> serviceType : services) {

            String serviceName = serviceType.getSimpleName();

            Path servicePath = getServicePath(targetPath, servicePackageName, serviceType);
            Path sourceServicePath = getServicePath(sourcePath, servicePackageName, serviceType);
            Path resourceServicePath = getServicePath(resourcePath, servicePackageName, serviceType);

            Path abstractPath = servicePath.resolve("Generated" + serviceName + classSuffix + ".java");
            Path concreteExistPath = sourceServicePath.resolve(serviceName + classSuffix + ".java");
            Path resourceExistPath = resourceServicePath.resolve(serviceName + ".properties");
            FixturesProperties properties = new FixturesProperties();
            properties.load(resourceExistPath);

            ClassDescription model = createModel(serviceTypeClass, serviceType, properties);
            if (isVerbose()) {
                getLog().info(String.format("Will generate for %s to %s", model.getServiceName(), abstractPath));
            }

            generate(mustache, abstractPath, model);
            if (Files.notExists(concreteExistPath)) {
                Path concretePath = generateConcreteToSource ? concreteExistPath : servicePath.resolve(serviceName + classSuffix + ".java");
                if (isVerbose()) {
                    getLog().info(String.format("Will generate for %s to %s", model.getServiceName(), concretePath));
                }
                generate(mustacheConcrete, concretePath, toConcrete(model));
            }
            properties.flush(resourceExistPath);
        }
    }

    ClassDescription toConcrete(ClassDescription model) {
        return new ClassDescription(
                model.getServiceProviderClass(),
                model.getPackageName(),
                model.getServiceName(),
                model.getClassSuffix(),
                model.getServiceSupportName(),
                model.getMethods(),
                new ImportManager().toDescription());
    }

    private <S> ClassDescription createModel(Class<?> contractType, Class<S> serviceType, FixturesProperties properties) {

        String servicePackageName = serviceType.getPackage().getName();

        Map<String, String> genericMapping = ServiceMapping.genericMapping(logDebug(), contractType, serviceType);
        ImportManager importManager = new ImportManager();
        List<MethodDescription> methods = new LinkedList<>();
        String serviceName = serviceType.getSimpleName();
        String servicePackage = getServicePackage(packageName, servicePackageName, contractType);
        String generatedServiceName = "Generated" + serviceName + classSuffix;
        MethodDescription.createMethodDescriptions(
                logInfo(),
                contractType,
                serviceType,
                methods,
                method -> {
                    String methodPrefix = method.getName() + ".";
                    MethodDescriptionImpl methodDescription = new MethodDescriptionImpl(servicePackage + "." + generatedServiceName, importManager, method, genericMapping);
                    for (java.lang.reflect.Parameter parameter : methodDescription.getParameters()) {
                        properties.register(methodPrefix, parameter);
                    }
                    return methodDescription;
                });

        importManager.importType(serviceSupportType);
        importManager.importType(Assume.class);
        importManager.importType(Assert.class);
        if (serviceProviderName != null) {
            importManager.importType(serviceProviderName);
        }
        methods.sort(Comparator.comparing(MethodDescription::getName));
        return new ClassDescription(
                serviceProviderName,
                servicePackage,
                serviceName,
                classSuffix,
                serviceSupportTypeSimpleName,
                new LinkedHashSet<>(methods),
                importManager.toDescription());
    }
}
