package io.ultreia.java4all.http.maven.plugin;

/*-
 * #%L
 * Http :: Maven Plugin
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.http.spi.model.MethodDescription;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * To check on all methods use parameters and return types known, such as primitives, String and user defined JsonType.
 * <p>
 * This helps to be sure we did not miss to add a used type as Json aware...
 * <p>
 * Created at 24/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.2
 */
@Mojo(name = "check-services-types", threadSafe = true, defaultPhase = LifecyclePhase.COMPILE, requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME)
public class CheckServicesTypesMojo extends HttpMojoSupport {
    /**
     * Extra parameter types to accept.
     */
    @Parameter(required = true)
    private List<String> acceptedParameterTypes;
    /**
     * Extra parameter types to accept by assignableFrom method.
     */
    @Parameter
    private List<String> acceptedAssignableParameterTypes;
    /**
     * Extra return types to accept.
     */
    @Parameter(required = true)
    private List<String> acceptedReturnTypes;
    /**
     * Extra return types to accept by assignableFrom method.
     */
    @Parameter
    private List<String> acceptedAssignableReturnTypes;

    /**
     * Accept primitive types ?
     */
    @Parameter(defaultValue = "true")
    private boolean acceptPrimitive;
    /**
     * Accept enum types ?
     */
    @Parameter(defaultValue = "true")
    private boolean acceptEnum;

    private Set<Class<?>> acceptedParameterClasses;
    private Set<Class<?>> acceptedReturnClasses;
    private Set<Class<?>> inheritanceAcceptedParameterClasses;
    private Set<Class<?>> inheritanceAcceptedReturnClasses;
    private String prefix;
    private String prefix2;
    private String prefix3;
    private int serviceCount;
    private int methodCount;

    private void init() throws MojoExecutionException {

        acceptedParameterClasses = new LinkedHashSet<>();
        for (String extraType : acceptedParameterTypes) {
            acceptedParameterClasses.add(getClass(extraType));
        }
        acceptedReturnClasses = new LinkedHashSet<>();
        for (String extraType : acceptedReturnTypes) {
            acceptedReturnClasses.add(getClass(extraType));
        }
        inheritanceAcceptedParameterClasses = new LinkedHashSet<>();
        if (acceptedAssignableParameterTypes != null) {
            for (String extraType : acceptedAssignableParameterTypes) {
                inheritanceAcceptedParameterClasses.add(getClass(extraType));
            }
        }
        inheritanceAcceptedReturnClasses = new LinkedHashSet<>();
        if (acceptedAssignableReturnTypes != null) {
            for (String extraType : acceptedAssignableReturnTypes) {
                inheritanceAcceptedReturnClasses.add(getClass(extraType));
            }
        }
        serviceCount = methodCount = 0;
    }

    @Override
    protected void doAction() throws MojoExecutionException {
        init();
        Class<?> serviceTypeClass = getClass(getServiceType());
        List<Class<?>> services = getServices(serviceTypeClass);
        for (Class<?> service : services) {
            checkService(serviceTypeClass, service);
        }
        getLog().info(String.format("Check on %d method(s) on %d service(s).", methodCount, serviceCount));
    }

    private void checkService(Class<?> serviceTypeClass, Class<?> service) throws MojoExecutionException {
        serviceCount++;
        getLog().debug(prefix = "Check service " + service.getName());
        List<Method> methods = MethodDescription.getDeclaredMethods(serviceTypeClass, service);
        for (Method method : methods) {
            checkMethod(service, method);

        }
    }

    private void checkMethod(Class<?> service, Method method) throws MojoExecutionException {
        methodCount++;
        getLog().debug(prefix2 = String.format("%s, method %s", prefix, method.getName()));
        checkReturnType(service, method, method.getGenericReturnType());
        for (java.lang.reflect.Parameter parameter : method.getParameters()) {
            checkParameter(service, method, parameter);
        }
    }

    private void checkReturnType(Class<?> service, Method method, Type returnType) throws MojoExecutionException {
        getLog().debug(prefix3 = String.format("%s return type %s", prefix2, returnType));
        if (void.class.equals(returnType)) {
            return;
        }
        if (Void.class.equals(returnType)) {
            return;
        }
        if (checkReturnType(returnType)) {
            throw new MojoExecutionException(String.format("Return type %s of method %s.%s is not accepted.", returnType.getTypeName(), service.getName(), method.getName()));
        }
    }

    private void checkParameter(Class<?> service, Method method, java.lang.reflect.Parameter parameter) throws MojoExecutionException {
        Type type = parameter.getParameterizedType();

        getLog().debug(prefix3 = String.format("%s parameter %s %s", prefix2, parameter.getName(), type));
        if (checkParameterType(type)) {
            throw new MojoExecutionException(String.format("Parameter %s type %s of method %s.%s is not accepted.", parameter.getName(), type.getTypeName(), service.getName(), method.getName()));
        }
    }

    private boolean checkParameterType(Type type) {
        getLog().debug(String.format("%s - Check parameter type %s", prefix3, type));
        Class<?> clazz;
        if (type instanceof Class<?>) {
            clazz = (Class<?>) type;
            if (clazz.isPrimitive() && acceptPrimitive) {
                return false;
            }
            if (clazz.isEnum() && acceptEnum) {
                return false;
            }
            if (acceptedParameterClasses.contains(clazz)) {
                return false;
            }

            for (Class<?> acceptedClass : inheritanceAcceptedParameterClasses) {
                if (acceptedClass.isAssignableFrom(clazz)) {
                    return false;
                }
            }
            return true;
        }
        if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            if (checkParameterType(parameterizedType.getRawType())) {
                return true;
            }
            Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
            for (Type actualTypeArgument : actualTypeArguments) {
                if (checkParameterType(actualTypeArgument)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean checkReturnType(Type type) {
        getLog().debug(String.format("%s - Check return type %s", prefix3, type));
        Class<?> clazz;
        if (type instanceof Class<?>) {
            clazz = (Class<?>) type;
            if (clazz.isPrimitive() && acceptPrimitive) {
                return false;
            }
            if (clazz.isEnum() && acceptEnum) {
                return false;
            }
            if (acceptedReturnClasses.contains(clazz)) {
                return false;
            }

            for (Class<?> acceptedClass : inheritanceAcceptedReturnClasses) {
                if (acceptedClass.isAssignableFrom(clazz)) {
                    return false;
                }
            }
            return true;
        }
        if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            if (checkReturnType(parameterizedType.getRawType())) {
                return true;
            }
            Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
            for (Type actualTypeArgument : actualTypeArguments) {
                if (checkReturnType(actualTypeArgument)) {
                    return true;
                }
            }
        }
        return false;
    }
}
