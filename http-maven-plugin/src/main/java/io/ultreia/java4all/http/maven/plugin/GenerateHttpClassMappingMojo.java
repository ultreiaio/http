package io.ultreia.java4all.http.maven.plugin;

/*-
 * #%L
 * Http :: Maven Plugin
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.classmapping.ImmutableClassMappingInstance;
import io.ultreia.java4all.http.HRestClientService;
import io.ultreia.java4all.http.spi.SpiHelper;
import io.ultreia.java4all.http.spi.model.ClassMappingDescription;
import io.ultreia.java4all.http.spi.model.ClassMappingTestDescription;
import io.ultreia.java4all.http.spi.model.ImportManager;
import io.ultreia.java4all.http.spi.model.ServiceMapping;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.junit.Assert;
import org.junit.Test;

import javax.annotation.Generated;
import java.io.File;
import java.nio.file.Path;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Generate a class mapping for services.
 * <p>
 * Created by tchemit on 07/09/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@Mojo(name = "generate-class-mapping", threadSafe = true, defaultPhase = LifecyclePhase.GENERATE_SOURCES, requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME)
public class GenerateHttpClassMappingMojo extends HttpMojoSupport {

    /**
     * Where to generate classes.
     */
    @Parameter(defaultValue = "${project.build.directory}/generated-sources/http")
    private File outputDirectory;

    /**
     * Where are the sources (used to detect if we should generate concrete classes).
     */
    @Parameter(defaultValue = "${project.basedir}/src/main/java", required = true)
    private File sourcesDirectory;
    /**
     * Where are the tests.
     */
    @Parameter(defaultValue = "${project.build.directory}/generated-test-sources/http", required = true)
    private File testsDirectory;

    /**
     * Suffix to add to generated classes name.
     */
    @Parameter(required = true)
    private String classSuffix;

    /**
     * Package name where to generate.
     */
    @Parameter(required = true)
    private String packageName;

    /**
     * Service support (must implements {@link HRestClientService}).
     */
    @Parameter(required = true)
    private String serviceSupportType;

    @Override
    protected void doAction() throws MojoExecutionException {
        Class<?> serviceTypeClass = getClass(serviceType);

        String serviceSupportTypeSimpleName = serviceSupportType.substring(serviceSupportType.lastIndexOf(".") + 1);
        String serviceSupportTypePackageName = serviceSupportType.substring(0, serviceSupportType.length() - serviceSupportTypeSimpleName.length() - 1);

        Path targetPath = getTargetPath(outputDirectory, packageName);
        Path testPath = getTargetPath(testsDirectory, packageName);

        getLog().debug(String.format("Will generate in: %s", targetPath));

        project.addCompileSourceRoot(outputDirectory.toString());
        project.addTestCompileSourceRoot(testPath.toString());

        List<Class<?>> services = getServices(serviceTypeClass);
        generateClassMapping(packageName,
                             serviceTypeClass,
                             targetPath,
                             testPath,
                             serviceSupportType,
                             classSuffix,
                             serviceSupportTypePackageName,
                             serviceSupportTypeSimpleName,
                             services);
    }

    private void generateClassMapping(String packageName,
                                      Class<?> serviceTypeClass,
                                      Path targetPath,
                                      Path testPath,
                                      String serviceSupportType,
                                      String classSuffix,
                                      String serviceSupportTypePackageName,
                                      String serviceSupportTypeSimpleName,
                                      List<Class<?>> services) throws MojoExecutionException {

        ImportManager importManager = new ImportManager();

        importManager.importType(Generated.class);
        importManager.importType(ImmutableClassMappingInstance.class);
        importManager.importType(serviceTypeClass);
        importManager.importType(serviceSupportType);
        importManager.importType(Set.class);
        importManager.importType(LinkedHashSet.class);
        importManager.importType(Collections.class);

        ImportManager testImportManager = new ImportManager();
        testImportManager.importType(Generated.class);
        testImportManager.importType(Test.class);
        testImportManager.importType(Assert.class);
        Set<ServiceMapping> servicesMap = new LinkedHashSet<>();

        services.forEach(s -> {
            String packageSuffix = s.getPackage().getName().substring(serviceTypeClass.getPackage().getName().length());
            String className = serviceSupportTypePackageName + packageSuffix + "." + s.getSimpleName() + classSuffix;
            boolean anonymous = SpiHelper.anonymous(s);
            servicesMap.add(new ServiceMapping(s, className, anonymous));
        });

        ClassMappingDescription classMappingModel = new ClassMappingDescription(
                packageName,
                classSuffix,
                serviceTypeClass.getSimpleName(),
                serviceSupportTypeSimpleName,
                getClass().getSimpleName(),
                importManager.toDescription(),
                servicesMap);

        Path resolve = targetPath.resolve("ClassMapping" + classSuffix + ".java");
        if (isVerbose()) {
            getLog().info(String.format("Will generate ClassMapping to %s", resolve));
        }
        generate(getMustache(getClass(), "ClassMapping"), resolve, classMappingModel);

        ClassMappingTestDescription classMappingTestModel = new ClassMappingTestDescription(
                packageName,
                classSuffix,
                getClass().getSimpleName(),
                testImportManager.toDescription(),
                servicesMap);

        Path testResolve = testPath.resolve("ClassMapping" + classSuffix + "Test.java");
        if (isVerbose()) {
            getLog().info(String.format("Will generate ClassMappingTest to %s", testResolve));
        }
        generate(getMustache(getClass(), "ClassMappingTest"), testResolve, classMappingTestModel);
    }

}
