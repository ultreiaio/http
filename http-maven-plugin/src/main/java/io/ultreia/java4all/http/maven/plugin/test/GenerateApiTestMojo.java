package io.ultreia.java4all.http.maven.plugin.test;

/*-
 * #%L
 * Http :: Maven Plugin
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.github.mustachejava.Mustache;
import io.ultreia.java4all.http.maven.plugin.HttpMojoSupport;
import io.ultreia.java4all.http.spi.model.ClassDescription;
import io.ultreia.java4all.http.spi.model.ImportManager;
import io.ultreia.java4all.http.spi.model.MethodDescription;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;

import java.io.File;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;

/**
 * To generate API Test contracts.
 * <p>
 * Created on 31/01/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.26
 */
@Mojo(name = "generate-api-test", threadSafe = true, defaultPhase = LifecyclePhase.GENERATE_SOURCES, requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME)
public class GenerateApiTestMojo extends HttpMojoSupport {
    /**
     * Where to generate classes.
     */
    @Parameter(defaultValue = "${project.build.directory}/generated-sources/http")
    private File outputDirectory;

    /**
     * Suffix to add to generated classes name.
     */
    @Parameter(required = true, defaultValue = "Test")
    private String classSuffix;

    /**
     * Package name where to generate.
     */
    @Parameter(required = true)
    private String packageName;
    /**
     * Optional service provider name to use in generated files.
     */
    @Parameter(property = "http.serviceProviderName")
    private String serviceProviderName;

    @Override
    protected void doAction() throws MojoExecutionException {
        Class<?> serviceTypeClass = getClass(serviceType);
        Path targetPath = getTargetPath(outputDirectory, packageName);

        getLog().debug(String.format("Will generate in: %s", targetPath));

        project.addCompileSourceRoot(outputDirectory.toString());

        Mustache mustache = getMustache(getClass(), "");
        String servicePackageName = serviceTypeClass.getPackage().getName();

        List<Class<?>> services = getServices(serviceTypeClass);

        for (Class<?> serviceType : services) {

            String serviceName = serviceType.getSimpleName();
            Path servicePath = getServicePath(targetPath, servicePackageName, serviceType);

            ClassDescription model = createModel(serviceTypeClass, serviceType);

            generate(servicePath, serviceName, mustache, model.toReadModel().orElse(null));
            generate(servicePath, serviceName, mustache, model.toWriteModel().orElse(null));
        }
    }

    protected void generate(Path servicePath, String serviceName, Mustache mustache, ClassDescription model) throws MojoExecutionException {
        if (model == null) {
            return;
        }
        Path targetPath = servicePath.resolve(serviceName + model.getReadOrWrite() + model.getClassSuffix() + ".java");
        if (isVerbose()) {
            getLog().info(String.format("Will generate for %s to %s", model.getServiceName(), targetPath));
        }
        super.generate(mustache, targetPath, model);
    }

    private <S> ClassDescription createModel(Class<?> contractType, Class<S> serviceType) {

        String servicePackageName = serviceType.getPackage().getName();

        ImportManager importManager = new ImportManager();
        if (serviceProviderName != null) {
            importManager.importType(serviceProviderName);
        }
        List<MethodDescription> methods = new LinkedList<>();
        MethodDescription.createMethodDescriptions(logInfo(),
                                                   contractType,
                                                   serviceType,
                                                   methods,
                                                   MethodDescriptionImpl::new);

        String serviceName = serviceType.getSimpleName();
        String servicePackage = getServicePackage(packageName, servicePackageName, contractType);

        importManager.importType(javax.annotation.Generated.class);

        methods.sort(Comparator.comparing(MethodDescription::getName));
        return new ClassDescription(
                serviceProviderName,
                servicePackage,
                serviceName,
                classSuffix,
                "",
                new LinkedHashSet<>(methods),
                importManager.toDescription());
    }


}
