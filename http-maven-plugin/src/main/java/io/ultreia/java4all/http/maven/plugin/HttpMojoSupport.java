package io.ultreia.java4all.http.maven.plugin;

/*-
 * #%L
 * Http :: Maven Plugin
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import io.ultreia.java4all.http.spi.model.ClassDescription;
import io.ultreia.java4all.http.spi.model.ImportManager;
import io.ultreia.java4all.http.spi.model.MethodDescription;
import io.ultreia.java4all.http.spi.model.ServiceMapping;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

/**
 * Created by tchemit on 13/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class HttpMojoSupport extends AbstractMojo {

    /**
     * The Maven Project Object
     *
     * @since 1.0
     */
    @Parameter(defaultValue = "${project}", readonly = true)
    protected
    MavenProject project;

    /**
     * Type of service to scan.
     *
     * @since 1.0
     */
    @Parameter(property = "http.serviceType", required = true)
    protected
    String serviceType;

    /**
     * Verbose log mode.
     *
     * @since 1.0.14
     */
    @Parameter(property = "http.verbose")
    private boolean verbose;

    private MustacheFactory mf;

    @Deprecated
    public static <S> List<Method> getDeclaredMethods(Class<?> contractType, Class<S> serviceType) {
        return MethodDescription.getDeclaredMethods(contractType, serviceType);
    }

    protected Path getTargetPath(File outputDirectory, String packageName) throws MojoExecutionException {
        Path targetPath = outputDirectory.toPath();
        for (String s : (packageName).split("\\.")) {
            targetPath = targetPath.resolve(s);
        }
        if (!Files.exists(targetPath)) {
            try {
                Files.createDirectories(targetPath);
            } catch (IOException e) {

                throw new MojoExecutionException("Can't create directory " + targetPath, e);
            }
        }

        return targetPath;
    }

    protected Class<?> getClass(String serviceType) throws MojoExecutionException {
        try {
            return serviceType == null ? null : Class.forName(serviceType, true, Thread.currentThread().getContextClassLoader());
        } catch (ClassNotFoundException e) {
            throw new MojoExecutionException("Can't find class " + serviceType, e);
        }
    }

    protected Mustache getMustache(Class<?> type, String suffix) {
        if (mf == null) {
            mf = new DefaultMustacheFactory("templates");
        }
        return mf.compile(type.getSimpleName() + suffix + ".mustache");
    }

    protected Path getServicePath(Path targetPath, String servicePackageName, Class<?> serviceType) {
        String servicePackage = serviceType.getPackage().getName().substring(servicePackageName.length());
        if (!servicePackage.isEmpty()) {
            for (String s : servicePackage.split("\\.")) {
                targetPath = targetPath.resolve(s);
            }
        }
        return targetPath;

    }

    protected String getServicePackage(String packageName, String servicePackageName, Class<?> contractType) {
        String servicePackage = servicePackageName.substring(contractType.getPackage().getName().length());
        return packageName + servicePackage;

    }

    protected void generate(Mustache mustache, Path targetPath, Object model) throws MojoExecutionException {
        try {
            if (!Files.exists(targetPath.getParent())) {
                Files.createDirectories(targetPath.getParent());
            }
            try (BufferedWriter writer = Files.newBufferedWriter(targetPath)) {
                mustache.execute(writer, model);
            }
        } catch (IOException e) {
            throw new MojoExecutionException("Can't generate at " + targetPath, e);
        }
    }

    public boolean isVerbose() {
        return verbose;
    }

    ClassDescription toConcrete(ClassDescription model) {
        ImportManager importManager = new ImportManager();

        importManager.importType(javax.annotation.Generated.class);

        return new ClassDescription(
                model.getServiceProviderClass(),
                model.getPackageName(),
                model.getServiceName(),
                model.getClassSuffix(),
                model.getServiceSupportName(),
                model.getMethods(),
                importManager.toDescription());
    }

    protected List<Class<?>> getServices(Class<?> serviceTypeClass) {
        return ServiceMapping.getAllServices(serviceTypeClass);
    }

    public MavenProject getProject() {
        return project;
    }

    public String getServiceType() {
        return serviceType;
    }

    protected Consumer<String> logInfo() {
        return isVerbose() ? getLog()::info : s -> {
        };
    }

    protected Consumer<String> logDebug() {
        return getLog()::debug;
    }

    @Override
    public void execute() throws MojoExecutionException {
        ClassLoader oldClassLoader = setClassLoader();
        try {
            doAction();
        } finally {
            if (oldClassLoader != null) {
                Thread.currentThread().setContextClassLoader(oldClassLoader);
            }
        }
    }

    protected abstract void doAction() throws MojoExecutionException;

    protected ClassLoader setClassLoader() throws MojoExecutionException {

        URLClassLoader newClassLoader;
        try {
            newClassLoader = initClassLoader();
        } catch (MalformedURLException e) {
            throw new MojoExecutionException(e);
        }
        ClassLoader oldClassLoader = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(newClassLoader);
        return oldClassLoader;
    }

    /**
     * Init mojo classLoader.
     *
     * @return the new classLoader
     * @throws MalformedURLException if an url was not correct
     */
    protected URLClassLoader initClassLoader(
    ) throws MalformedURLException {
        MavenProject project = getProject();
        URLClassLoader result;
        try {
            Set<String> doneSet = new HashSet<>();
            List<URL> lUrls = new ArrayList<>();

            addDirectoryToUrlsList(new File(project.getBuild().getOutputDirectory()), lUrls, doneSet);

            getLog().info("use project compile scope class-path");
            // add also all dependencies of the project in compile scope
            Set<?> artifacts = project.getArtifacts();
            for (Object o : artifacts) {
                Artifact a = (Artifact) o;
                addDirectoryToUrlsList(a.getFile(), lUrls, doneSet);
            }
            result = new URLClassLoader(lUrls.toArray(new URL[0]), getClass().getClassLoader());
        } catch (IOException e) {
            throw new RuntimeException("Can't create ClassLoader for reason " + e.getMessage(), e);
        }
        if (isVerbose()) {
            for (URL entry : result.getURLs()) {
                getLog().info("classpath : " + entry);
            }
        }
        return result;
    }

    /**
     * Add the given {@code directory} in {@code urls} if not already included.
     * <p>
     * <b>Note:</b> We use a extra list to store file string representation,
     * since we do NOT want any url resolution and the {@link URL#equals(Object)} is doing some...
     *
     * @param directory the directory to insert in {@code urls}
     * @param urls      list of urls
     * @param done      list of string representation of urls
     * @throws MalformedURLException if pb while converting file to url
     */
    protected void addDirectoryToUrlsList(File directory, List<URL> urls, Set<String> done) throws MalformedURLException {
        // do the comparison on a String to avoid url to be resolved (in URL.equals method)
        addUrlToUrlsList(directory.toURI().toURL(), urls, done);
    }

    /**
     * Add the given {@code url} in {@code urls} if not already included.
     * <p>
     * <b>Note:</b> We use a extra list to store file string representation,
     * since we do NOT want any url resolution and the
     * {@link URL#equals(Object)} is doing some..
     *
     * @param url  the url to insert in {@code urls}
     * @param urls list of urls
     * @param done list of string representation of urls
     */
    protected void addUrlToUrlsList(URL url, List<URL> urls, Set<String> done) {
        // do the comparison on a String to avoid url to be resolved (in URL.equals method)
        String u = url.toString();
        if (!done.contains(u)) {
            done.add(u);
            urls.add(url);
        }
    }
}
