package io.ultreia.java4all.http.maven.plugin.test;

/*-
 * #%L
 * Http :: Maven Plugin
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.http.spi.SpiHelper;
import io.ultreia.java4all.http.spi.model.MethodDescription;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Created on 03/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class MethodDescriptionImpl implements MethodDescription {

    private final String name;
    private final boolean write;

    MethodDescriptionImpl(Method method) {
        this.name = Objects.requireNonNull(method).getName();
        this.write = SpiHelper.write(method);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getReturnType() {
        return "void";
    }

    @Override
    public String getParametersDefinition() {
        return "";
    }

    @Override
    public List<Class<?>> getExceptions() {
        return Collections.emptyList();
    }

    @Override
    public boolean isWrite() {
        return write;
    }
}
