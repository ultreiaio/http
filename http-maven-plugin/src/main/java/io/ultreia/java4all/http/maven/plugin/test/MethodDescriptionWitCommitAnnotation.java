package io.ultreia.java4all.http.maven.plugin.test;

/*-
 * #%L
 * Http :: Maven Plugin
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.lang.reflect.Method;

/**
 * Created on 12/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.36
 */
public class MethodDescriptionWitCommitAnnotation extends MethodDescriptionImpl {

    private final String commitAnnotationText;

    MethodDescriptionWitCommitAnnotation(Method method, String commitAnnotationText) {
        super(method);
        this.commitAnnotationText = commitAnnotationText;
    }

    public String getCommitAnnotation() {
        return isWrite() ? commitAnnotationText : "";
    }
}
