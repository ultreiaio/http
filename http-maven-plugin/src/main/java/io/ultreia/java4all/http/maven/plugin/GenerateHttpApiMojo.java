package io.ultreia.java4all.http.maven.plugin;

/*-
 * #%L
 * Http :: Maven Plugin
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.github.mustachejava.Mustache;
import io.ultreia.java4all.http.HRestApiService;
import io.ultreia.java4all.http.spi.model.ClassDescription;
import io.ultreia.java4all.http.spi.model.ImportManager;
import io.ultreia.java4all.http.spi.model.MethodDescription;
import io.ultreia.java4all.http.spi.model.MethodDescriptionImpl;
import io.ultreia.java4all.http.spi.model.ServiceMapping;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Generate Rest Api services.
 * <p>
 * Created by tchemit on 13/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@Mojo(name = "generate-api", threadSafe = true, defaultPhase = LifecyclePhase.GENERATE_SOURCES, requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME)
public class GenerateHttpApiMojo extends HttpMojoSupport {

    /**
     * Where to generate classes.
     */
    @Parameter(defaultValue = "${project.build.directory}/generated-sources/http", required = true)
    private File outputDirectory;

    /**
     * Where are the sources (used to detect if we should generate concrete classes).
     */
    @Parameter(defaultValue = "${project.basedir}/src/main/java", required = true)
    private File sourcesDirectory;

    /**
     * Suffix to add to generated classes name.
     */
    @Parameter(required = true, defaultValue = "RestApi")
    private String classSuffix;

    /**
     * Service support (must implements {@link HRestApiService}).
     */
    @Parameter(required = true)
    private String serviceSupportType;

    /**
     * Package name where to generate.
     */
    @Parameter(required = true)
    private String packageName;

    private String serviceSupportTypeSimpleName;

    @Override
    protected void doAction() throws MojoExecutionException {

        Class<?> serviceTypeClass = getClass(serviceType);

        serviceSupportTypeSimpleName = serviceSupportType.substring(serviceSupportType.lastIndexOf(".") + 1);

        Path targetPath = getTargetPath(outputDirectory, packageName);
        Path sourcePath = getTargetPath(sourcesDirectory, packageName);

        getLog().debug(String.format("Will generate in: %s", targetPath));

        project.addCompileSourceRoot(outputDirectory.toString());

        Mustache mustache = getMustache(getClass(), "Generated");
        Mustache mustacheConcrete = getMustache(getClass(), "Concrete");

        List<Class<?>> services = getServices(serviceTypeClass);

        String servicePackageName = serviceTypeClass.getPackage().getName();

        for (Class<?> serviceType : services) {

            String serviceName = serviceType.getSimpleName();

            ClassDescription model = createModel(serviceTypeClass, serviceType);

            Path servicePath = getServicePath(targetPath, servicePackageName, serviceType);
            Path existServicePath = getServicePath(sourcePath, servicePackageName, serviceType);

            Path abstractPath = servicePath.resolve("Generated" + serviceName + classSuffix + ".java");
            Path concretePath = servicePath.resolve(serviceName + classSuffix + ".java");
            Path concreteExistPath = existServicePath.resolve(serviceName + classSuffix + ".java");
            if (isVerbose()) {
                getLog().info(String.format("Will generate for %s to %s", model.getServiceName(), abstractPath));
            }

            generate(mustache, abstractPath, model);

            if (!Files.exists(concreteExistPath)) {
                if (isVerbose()) {
                    getLog().info(String.format("Will generate for %s to %s", model.getServiceName(), concretePath));
                }

                generate(mustacheConcrete, concretePath, toConcrete(model));

            }

        }

    }

    private <S> ClassDescription createModel(Class<?> contractType, Class<S> serviceType) {

        String servicePackageName = serviceType.getPackage().getName();

        Map<String, String> genericMapping = ServiceMapping.genericMapping(logDebug(), contractType, serviceType);

        ImportManager importManager = new ImportManager();

        String serviceName = serviceType.getSimpleName();
        String servicePackage = getServicePackage(packageName, servicePackageName, contractType);
        String generatedServiceName = "Generated" + serviceName + classSuffix;
        Set<MethodDescription> methods = new LinkedHashSet<>();
        MethodDescription.createMethodDescriptions(logInfo(),
                                                   contractType,
                                                   serviceType,
                                                   methods,
                                                   method -> new MethodDescriptionImpl(servicePackage + "." + generatedServiceName, importManager, method, genericMapping));

        importManager.importType(serviceSupportType);
        importManager.importType(serviceType.getPackage().getName() + "." + serviceName);
        importManager.importType(javax.annotation.Generated.class);

        return new ClassDescription(
                null,
                servicePackage,
                serviceName,
                classSuffix,
                serviceSupportTypeSimpleName,
                methods,
                importManager.toDescription());
    }


}
