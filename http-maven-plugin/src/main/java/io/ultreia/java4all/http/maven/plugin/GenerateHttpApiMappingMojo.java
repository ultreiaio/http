package io.ultreia.java4all.http.maven.plugin;

/*
 * #%L
 * Http :: Maven Plugin
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.github.mustachejava.Mustache;
import io.ultreia.java4all.http.spi.RequestAnnotation;
import io.ultreia.java4all.http.spi.SpiHelper;
import io.ultreia.java4all.http.spi.model.MethodDescription;
import io.ultreia.java4all.lang.Strings;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

/**
 * Generate rest api mapping file (using WebMotion).
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@Mojo(name = "generate-api-mapping", defaultPhase = LifecyclePhase.GENERATE_RESOURCES, requiresDependencyResolution = ResolutionScope.COMPILE)
public class GenerateHttpApiMappingMojo extends HttpMojoSupport {

    /**
     * Location of mapping file.
     */
    @Parameter(defaultValue = "${project.basedir}/src/main/filtered-resources/mapping", required = true)
    private File mappingFile;

    /**
     * Suffix to add to generated classes name.
     */
    @Parameter(required = true, defaultValue = "RestApi")
    private String classSuffix;

    /**
     * Prefix to add to rest api.
     */
    @Parameter(required = true)
    private String pathPrefix;

    /**
     * Prefix to add to package.
     */
    @Parameter
    private String packagePrefix;
    /**
     * To not add prefix in mapping.
     */
    @Parameter
    private boolean skipPrefix;

    static class ModelInformation {
        private final String staticContent;
        private final List<MethodInformation> methods;
        private final String pathPrefix;
        private final String classSuffix;

        ModelInformation(String staticContent, List<MethodInformation> methods, String pathPrefix, String classSuffix) {
            this.staticContent = staticContent;
            this.methods = methods;
            this.pathPrefix = pathPrefix;
            this.classSuffix = classSuffix;
        }

        public List<MethodInformation> getMethods() {
            return methods;
        }

        public String getStaticContent() {
            return staticContent;
        }

        public String getPathPrefix() {
            return pathPrefix;
        }

        public String getClassSuffix() {
            return classSuffix;
        }


    }

    static class MethodInformation implements Comparable<MethodInformation> {

        static int maxRequestTypeLength = 0;
        static int maxMethodNameLength = 0;
        private final String requestType;
        private final String servicePackage;
        private final String serviceName;
        private final String methodName;
        private final String pathPrefix;
        private final String classSuffix;

        MethodInformation(RequestAnnotation requestAnnotation, String servicePackage, String serviceName, String methodName, String pathPrefix, String classSuffix) {
            this.requestType = requestAnnotation.getMethod().toUpperCase();
            this.servicePackage = servicePackage;
            this.serviceName = serviceName;
            this.methodName = methodName;
            this.pathPrefix = pathPrefix;
            this.classSuffix = classSuffix;
            maxRequestTypeLength = Math.max(maxRequestTypeLength, this.requestType.length());
            maxMethodNameLength = Math.max(maxMethodNameLength, getPath0().length());

        }

        private String getPath0() {
            return String.format("%s%s%s/%s", pathPrefix, servicePackage.replaceAll("\\.", "/"), serviceName, methodName);
        }

        public String getRequestType() {
            return Strings.rightPad(requestType, maxRequestTypeLength, ' ');
        }

        public String getServiceName() {
            return serviceName;
        }

        public String getPath() {
            return right(getPath0(), maxMethodNameLength);
        }

        public String getRule() {
            return String.format("%s %s%s%s.%s", Strings.rightPad(getPath0(), maxMethodNameLength, ' '), getServicePackage(), getServiceName(), classSuffix, getMethodNameTrim());
        }

        public String getMethodNameTrim() {
            return methodName;
        }

        public String getServicePackage() {
            return servicePackage;
        }

        @Override
        public int compareTo(MethodInformation o) {
            int result;
            result = getPath0().compareTo(o.getPath0());
            if (result != 0) {
                return result;
            }
            result = requestType.compareTo(o.getRequestType());
            if (result != 0) {
                return result;
            }
            return methodName.compareTo(o.getMethodNameTrim());
        }
    }

    //FIXME Move this to Strings
    public static String right(final String str, final int len) {
        if (str == null) {
            return null;
        }
        if (len < 0) {
            return "";
        }
        if (str.length() <= len) {
            return str;
        }
        return str.substring(str.length() - len);
    }

    @Override
    protected void doAction() throws MojoExecutionException {

        if (!skipPrefix && packagePrefix != null && !packagePrefix.isEmpty()) {
            if (!packagePrefix.endsWith(".")) {
                packagePrefix += ".";
            }
        } else {
            packagePrefix = "";
        }
        if (skipPrefix) {
            pathPrefix = "/";
        }
        if (!pathPrefix.endsWith("/")) {
            pathPrefix += "/";
        }

        Path targetPath = getTargetPath(mappingFile.getParentFile(), "").resolve(mappingFile.getName());

        Class<?> serviceTypeClass = getClass(serviceType);

        if (isVerbose()) {
            getLog().info(String.format("Will generate in: %s", targetPath));
        }

        ModelInformation model;
        try {
            model = createModel(serviceTypeClass);
        } catch (IOException e) {
            throw new MojoExecutionException("Can't create model", e);
        }

        Mustache mustache = getMustache(getClass(), "");
        generate(mustache, targetPath, model);
    }

    private <S> ModelInformation createModel(Class<S> serviceTypeClass) throws IOException {
        String servicePackageName = serviceTypeClass.getPackage().getName();

        List<MethodInformation> methods = new LinkedList<>();


        for (Class<?> serviceType : getServices(serviceTypeClass)) {
            generate(servicePackageName, serviceTypeClass, serviceType, methods);
        }

        methods.sort(MethodInformation::compareTo);

        if (isVerbose()) {
            getLog().info(String.format("Max method name size: %d", MethodInformation.maxMethodNameLength));
        }
        String content = new String(Files.readAllBytes(mappingFile.toPath()));

        int indexOf = content.indexOf("# →→→ Generated dynamic mapping");
        content = content.substring(0, indexOf + "# →→→ Generated dynamic mapping".length());

        return new ModelInformation(content, methods, pathPrefix, classSuffix);
    }

    private <S> void generate(String servicePackageName, Class<?> contractType, Class<S> serviceType, List<MethodInformation> methods) throws IOException {

        String serviceName = serviceType.getSimpleName();

        String servicePackage = serviceType.getPackage().getName().substring(servicePackageName.length());
        if (!servicePackage.isEmpty()) {
            servicePackage += ".";
        }
        servicePackage = packagePrefix + Strings.removeStart(servicePackage, ".");
        String finalServicePackage = servicePackage;
        MethodDescription.createMethodDescriptions(
                logInfo(),
                contractType,
                serviceType,
                methods,
                method -> new MethodInformation(SpiHelper.getRequestAnnotation(method), finalServicePackage, serviceName, method.getName(), pathPrefix, classSuffix));

    }
}

