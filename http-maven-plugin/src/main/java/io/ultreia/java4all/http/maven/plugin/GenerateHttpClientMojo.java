package io.ultreia.java4all.http.maven.plugin;

/*-
 * #%L
 * Http :: Maven Plugin
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.github.mustachejava.Mustache;
import com.google.common.base.Joiner;
import com.google.gson.reflect.TypeToken;
import io.ultreia.java4all.http.HRequest;
import io.ultreia.java4all.http.HResponse;
import io.ultreia.java4all.http.HResponseErrorException;
import io.ultreia.java4all.http.HRestClientService;
import io.ultreia.java4all.http.spi.Nullable;
import io.ultreia.java4all.http.spi.RequestAnnotation;
import io.ultreia.java4all.http.spi.SpiHelper;
import io.ultreia.java4all.http.spi.model.ClassDescription;
import io.ultreia.java4all.http.spi.model.ImportManager;
import io.ultreia.java4all.http.spi.model.MethodDescription;
import io.ultreia.java4all.http.spi.model.ServiceMapping;
import io.ultreia.java4all.util.TimeLog;
import org.apache.hc.core5.http.HttpStatus;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;

import java.io.File;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Generate Rest client services.
 * <p>
 * Created by tchemit on 13/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@Mojo(name = "generate-client", threadSafe = true, defaultPhase = LifecyclePhase.GENERATE_SOURCES, requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME)
public class GenerateHttpClientMojo extends HttpMojoSupport {

    /**
     * Where to generate classes.
     */
    @Parameter(defaultValue = "${project.build.directory}/generated-sources/http")
    private File outputDirectory;

    /**
     * Where are the sources (used to detect if we should generate concrete classes).
     */
    @Parameter(defaultValue = "${project.basedir}/src/main/java", required = true)
    private File sourcesDirectory;

    /**
     * Suffix to add to generated classes name.
     */
    @Parameter(required = true, defaultValue = "RestClient")
    private String classSuffix;

    /**
     * Package name where to generate.
     */
    @Parameter(required = true)
    private String packageName;

    /**
     * Service support (must implements {@link HRestClientService}).
     */
    @Parameter(required = true)
    private String serviceSupportType;

    /**
     * Type of objects to serialize as gson.
     */
    @Parameter(required = true)
    private String gsonType;

    /**
     * Type of objects to serialize via the compactName() method.
     */
    @Parameter
    private String enumerationType;

    @Parameter(defaultValue = "false")
    private boolean addAuthenticationToken;

    private String serviceSupportTypeSimpleName;

    @SuppressWarnings("unused")
    public static class MethodDescriptionImpl implements MethodDescription {

        private static final String METHOD_BODY_PATTERN = "%s%s";
        private static final String RETURN_BODY_PATTERN = "" +
                "        t0 = TimeLog.getTime();\n" +
                "        try {\n" +
                "            %s%s;\n" +
                "        } finally {\n" +
                "            TIME_LOG.log(t0, \"consumeResponse\", hRequest.toString());\n" +
                "        }";
        private static final String REQUEST_BODY_PATTERN = "create(\"%s\")%s\n" +
                "                .%s()";
        private static final String RESPONSE_BODY_PATTERN = "" +
                "    long t0 = TimeLog.getTime();\n" +
                "        HRequest hRequest = %s;\n" +
                "        TIME_LOG.log(t0, \"createRequest\", hRequest.toString());\n" +
                "        t0 = TimeLog.getTime();\n" +
                "        HResponse response;\n" +
                "        try {\n" +
                "            response = executeRequest(hRequest, HttpStatus.SC_OK);\n" +
                "        } finally {\n" +
                "            TIME_LOG.log(t0, \"executeRequest\", hRequest.toString());\n" +
                "        }\n";
        private final String name;
        private final String returnType;
        private final String parametersDefinition;
        private final String checkParameters;
        private final String body;
        private final List<Class<?>> exceptions;
        private final boolean write;

        <S> MethodDescriptionImpl(String declaringClass, ImportManager importManager, Class<S> serviceType, Method method, Map<String, String> currentMapping, Class<?> gsonTypeClass, Class<?> enumerationTypeClass, boolean addAuthenticationToken) {
            this.name = method.getName();
            this.exceptions = MethodDescription.getExceptions(method, importManager);

            Set<String> genericDefinitionsOfReturnType = importManager.getGenericDefinitionsOfReturnType(method);
            String returnTypePrefix = importManager.getGenericDefinitionOfReturnType(method);
            this.write = SpiHelper.write(method);
            String simpleReturnType = importManager.importReturnType(method, currentMapping);
            this.returnType = returnTypePrefix + simpleReturnType;
            String genericType = null;

            String simpleReturnType1;
            {
                String genericReturnType = method.getGenericReturnType().getTypeName();
                int indexOf = genericReturnType.indexOf("<");
                if (indexOf > -1) {

                    genericType = genericReturnType.substring(indexOf + 1, genericReturnType.length() - 1);
                    if (genericDefinitionsOfReturnType.contains(genericType)) {

                        simpleReturnType1 = importManager.removeGeneric(simpleReturnType);
                    } else {
                        simpleReturnType1 = importManager.removeGeneric(importManager.importGenericParts(genericType));
                        if (currentMapping != null && currentMapping.containsKey(simpleReturnType1)) {
                            simpleReturnType1 = currentMapping.get(simpleReturnType1);
                        }
                    }

                } else {

                    simpleReturnType1 = importManager.removeGeneric(simpleReturnType);

                }
            }
            StringBuilder requestParamsBuilder = new StringBuilder();
            StringBuilder parametersDefinitionBuilder = new StringBuilder();
            StringBuilder checkParametersBuilder = new StringBuilder();
            for (java.lang.reflect.Parameter parameter : method.getParameters()) {
                String name = parameter.getName();
                boolean nullable = SpiHelper.nullable(parameter);
                parametersDefinitionBuilder.append(", ");
                if (nullable) {
                    importManager.importType(Nullable.class);
                    parametersDefinitionBuilder.append("@").append(Nullable.class.getSimpleName()).append(" ");
                } else if (!parameter.getType().isPrimitive()) {
                    importManager.importType(Objects.class);
                    checkParametersBuilder.append(String.format("        Objects.requireNonNull(%1$s, \"Parameter '%1$s' (in method %2$s#%3$s) can not be null.\");\n", name, declaringClass, this.name));
                }
                parametersDefinitionBuilder.append(importManager.importParameterType(parameter, currentMapping)).append(" ").append(name);

                String methodName = "addParameter";
                String value;
                if (gsonTypeClass.isAssignableFrom(parameter.getType())) {
                    if (nullable) {
                        importManager.importType(Optional.class);
                        value = String.format("Optional.ofNullable(%s).map(gson()::toJson).orElse(null)", name);
                    } else {
                        value = String.format("gson().toJson(%s)", name);
                    }
                } else if (enumerationTypeClass != null && enumerationTypeClass.isAssignableFrom(parameter.getType())) {
                    value = name + ".compactName()";
                } else {
                    value = name;
                }
                Type parameterizedType = parameter.getParameterizedType();
                if (Iterable.class.isAssignableFrom(parameter.getType()) || Map.class.isAssignableFrom(parameter.getType())) {

                    if (nullable) {
                        importManager.importType(Optional.class);
                        value = String.format("Optional.ofNullable(%s).map(gson()::toJson).orElse(null)", name);
                    } else {
                        value = String.format("gson().toJson(%s)", name);
                    }
                    requestParamsBuilder.append(String.format("\n                .%s(\"%s\", %s)", methodName, name, value));

                } else {

                    requestParamsBuilder.append(String.format("\n                .%s(\"%s\", %s)", methodName, name, value));
                    if (parameterizedType instanceof TypeVariable<?>) {
                        TypeVariable<?> typeVariable = (TypeVariable<?>) parameterizedType;
                        requestParamsBuilder.append(String.format("\n                .addParameter(\"%s\", %s.getClass().getCanonicalName())", HRestClientService.PARAMETERIZED_TYPE_PREFIX + typeVariable.getName(), name));
                    }
                }

            }

            String parametersDefinition = "";
            if (parametersDefinitionBuilder.length() > 0) {
                parametersDefinition = parametersDefinitionBuilder.substring(2);
            }
            this.parametersDefinition = parametersDefinition;

            String returnInvocation;
            String returnTransformation;

            if (returnType.contains("void")) {
                returnInvocation = "";
                returnTransformation = "";
            } else {
                returnInvocation = "return ";
                boolean checkGeneric = false;
                String importTypeAndRemoveGeneric = importManager.removeGeneric(simpleReturnType);
                switch (importTypeAndRemoveGeneric) {
                    case "boolean":
                        returnTransformation = ".toBoolean()";
                        break;
                    case "int":
                        returnTransformation = ".toInt()";
                        break;
                    case "long":
                        returnTransformation = ".toLong()";
                        break;
                    case "double":
                        returnTransformation = ".toDouble()";
                        break;
                    case "float":
                        returnTransformation = ".toFloat()";
                        break;

                    case "Map":
                    case "java.util.Map":
                        importManager.importType(TypeToken.class);
                        returnTransformation = ".toJson(new TypeToken<" + simpleReturnType + ">(){}.getType())";
                        break;
                    case "List":
                    case "java.util.List":
                        importManager.importType(List.class);
                        String genericReturnTransformation = null;
                        if (!genericDefinitionsOfReturnType.isEmpty()) {
                            for (String s : genericDefinitionsOfReturnType) {
                                if (Objects.equals(genericType, s)) {
                                    // try from parameters
                                    java.lang.reflect.Parameter[] parameters = method.getParameters();
                                    boolean foundType = false;
                                    if (parameters != null) {
                                        for (java.lang.reflect.Parameter parameter : parameters) {
                                            if (parameter.getParameterizedType().getTypeName().equals("java.lang.Class<" + genericType + ">")) {
                                                genericReturnTransformation = ".toList(" + parameter.getName() + ")";
                                                foundType = true;
                                                break;
                                            }
                                        }
                                    }
                                    if (foundType) {
                                        break;
                                    }
                                    importManager.importType(TypeToken.class);
                                    genericReturnTransformation = ".toList((Class)new TypeToken<" + genericType + ">(){}.getType())";
                                    break;
                                }
                            }
                        }
                        if (genericReturnTransformation == null) {
                            returnTransformation = ".toList(" + simpleReturnType1 + ".class)";
                        } else {
                            returnTransformation = genericReturnTransformation;
                        }
                        checkGeneric = true;
                        break;
                    case "Optional":
                    case "java.util.Optional":
                        importManager.importType(Optional.class);
                        returnTransformation = ".toOptional(" + simpleReturnType1 + ".class)";
                        checkGeneric = true;
                        break;
                    case "Set":
                    case "java.util.Set":
                        importManager.importType(Set.class);
                        genericReturnTransformation = null;
                        if (!genericDefinitionsOfReturnType.isEmpty()) {
                            for (String s : genericDefinitionsOfReturnType) {
                                if (Objects.equals(genericType, s)) {

                                    // try from parameters
                                    java.lang.reflect.Parameter[] parameters = method.getParameters();
                                    boolean foundType = false;
                                    if (parameters != null) {
                                        for (java.lang.reflect.Parameter parameter : parameters) {
                                            if (parameter.getParameterizedType().getTypeName().equals("java.lang.Class<" + genericType + ">")) {
                                                genericReturnTransformation = ".toSet(" + parameter.getName() + ")";
                                                foundType = true;
                                                break;
                                            }
                                        }
                                    }
                                    if (foundType) {
                                        break;
                                    }
                                    importManager.importType(TypeToken.class);
                                    genericReturnTransformation = ".toSet((Class)new TypeToken<" + genericType + ">(){}.getType())";
                                    break;
                                }
                            }
                        }
                        if (genericReturnTransformation == null) {
                            returnTransformation = ".toSet(" + simpleReturnType1 + ".class)";
                        } else {
                            returnTransformation = genericReturnTransformation;
                        }
                        checkGeneric = true;
                        break;
                    default:
                        if (genericDefinitionsOfReturnType.contains(importTypeAndRemoveGeneric)) {
                            java.lang.reflect.Parameter[] parameters = method.getParameters();
                            boolean foundType = false;
                            if (parameters != null) {
                                for (java.lang.reflect.Parameter parameter : parameters) {
                                    if (parameter.getParameterizedType().getTypeName().equals("java.lang.Class<" + importTypeAndRemoveGeneric + ">")) {
                                        importTypeAndRemoveGeneric = parameter.getName();
                                        foundType = true;
                                        break;
                                    }
                                }
                            }
                            if (!foundType) {
                                importTypeAndRemoveGeneric = method.getReturnType().getSimpleName() + ".class";
                            }
                        } else {
                            importTypeAndRemoveGeneric = importTypeAndRemoveGeneric + ".class";
                        }
                        returnTransformation = ".toJson(" + importTypeAndRemoveGeneric + ")";
                        break;
                }

                if (checkGeneric && genericType != null && genericType.contains("<")) {
                    returnInvocation += String.format("(%s) ", importTypeAndRemoveGeneric);
                }
            }

            RequestAnnotation requestAnnotation = SpiHelper.getRequestAnnotation(method);
            boolean useMultiPartForm = requestAnnotation.isUseMultiPartForm();

            String methodStr = requestAnnotation.getMethod();
            int timeOut = requestAnnotation.getTimeOut();

            if (addAuthenticationToken) {
                requestParamsBuilder.append("\n                .addAuthenticationToken()");
            }
            if (useMultiPartForm) {
                requestParamsBuilder.append("\n                .useMultiPartForm()");
            }
            if (timeOut > 0) {
                importManager.importType(TimeUnit.class);
                requestParamsBuilder.append(String.format("\n                .setTimeout(TimeUnit.MINUTES, %d)", timeOut));
            }
            checkParameters = checkParametersBuilder.toString();
            String url = method.getName();
            String request = String.format(REQUEST_BODY_PATTERN, url, requestParamsBuilder, methodStr.toLowerCase());
            String response = String.format(RESPONSE_BODY_PATTERN, request);
            String returnContent = returnInvocation.trim().isEmpty() ? "" : String.format(RETURN_BODY_PATTERN, returnInvocation, "response" + returnTransformation);

            String format = String.format(METHOD_BODY_PATTERN, response, returnContent);
            if (withExceptions()) {
                List<String> lines = Arrays.asList(format.split("\\n"));
                format = Joiner.on("\n").join(lines.stream().map(l -> "    " + l).collect(Collectors.toList()));
            }
            this.body = format;
        }

        @Override
        public boolean withExceptions() {
            // To unwrap runtime exceptions (See https://gitlab.com/ultreiaio/http/-/issues/53)
            return true;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public String getReturnType() {
            return returnType;
        }

        @Override
        public String getParametersDefinition() {
            return parametersDefinition;
        }

        @Override
        public List<Class<?>> getExceptions() {
            return exceptions;
        }

        @Override
        public boolean isWrite() {
            return write;
        }

        public String getBody() {
            return body;
        }

        public String getCheckParameters() {
            return checkParameters;
        }
    }

    @Override
    protected void doAction() throws MojoExecutionException {

        Class<?> serviceTypeClass = getClass(serviceType);
        Class<?> gsonTypeClass = getClass(gsonType);
        Class<?> enumerationTypeClass = getClass(enumerationType);

        serviceSupportTypeSimpleName = serviceSupportType.substring(serviceSupportType.lastIndexOf(".") + 1);

        Path targetPath = getTargetPath(outputDirectory, packageName);
        Path sourcePath = getTargetPath(sourcesDirectory, packageName);

        getLog().debug(String.format("Will generate in: %s", targetPath));

        project.addCompileSourceRoot(outputDirectory.toString());

        Mustache mustache = getMustache(getClass(), "Generated");
        Mustache mustacheConcrete = getMustache(getClass(), "Concrete");
        String servicePackageName = serviceTypeClass.getPackage().getName();

        List<Class<?>> services = getServices(serviceTypeClass);

        for (Class<?> serviceType : services) {

            String serviceName = serviceType.getSimpleName();

            ClassDescription model = createModel(serviceTypeClass, serviceType, gsonTypeClass, enumerationTypeClass);

            Path servicePath = getServicePath(targetPath, servicePackageName, serviceType);
            Path existServicePath = getServicePath(sourcePath, servicePackageName, serviceType);

            Path abstractPath = servicePath.resolve("Generated" + serviceName + classSuffix + ".java");
            Path concretePath = servicePath.resolve(serviceName + classSuffix + ".java");
            Path concreteExistPath = existServicePath.resolve(serviceName + classSuffix + ".java");

            if (isVerbose()) {
                getLog().info(String.format("Will generate for %s to %s", model.getServiceName(), abstractPath));
            }

            generate(mustache, abstractPath, model);

            if (!Files.exists(concreteExistPath)) {
                if (isVerbose()) {
                    getLog().info(String.format("Will generate for %s to %s", model.getServiceName(), concretePath));
                }

                generate(mustacheConcrete, concretePath, toConcrete(model));
            }
        }

    }

    private <S> ClassDescription createModel(Class<?> contractType, Class<S> serviceType, Class<?> gsonTypeClass, Class<?> enumerationTypeClass) {

        String servicePackageName = serviceType.getPackage().getName();

        Map<String, String> genericMapping = ServiceMapping.genericMapping(logDebug(), contractType, serviceType);

        ImportManager importManager = new ImportManager();
        String serviceName = serviceType.getSimpleName();
        String servicePackage = getServicePackage(packageName, servicePackageName, contractType);
        String generatedServiceName = "Generated" + serviceName + classSuffix;
        Set<MethodDescription> methods = new LinkedHashSet<>();
        boolean anonymous = SpiHelper.anonymous(serviceType);
        MethodDescription.createMethodDescriptions(logInfo(),
                                                   contractType,
                                                   serviceType,
                                                   methods,
                                                   method -> new MethodDescriptionImpl(servicePackage + "." + generatedServiceName, importManager, serviceType, method, genericMapping, gsonTypeClass, enumerationTypeClass, addAuthenticationToken && !anonymous));


        importManager.importType(serviceSupportType);
        importManager.importType(serviceType.getPackage().getName() + "." + serviceName);
        importManager.importType(javax.annotation.Generated.class);
        importManager.importType(TimeLog.class);
        importManager.importType(HRequest.class);
        importManager.importType(HResponse.class);
        importManager.importType(HResponseErrorException.class);
        importManager.importType(HttpStatus.class);
        return new ClassDescription(
                null,
                servicePackage,
                serviceName,
                classSuffix,
                serviceSupportTypeSimpleName,
                methods,
                importManager.toDescription());
    }


}
