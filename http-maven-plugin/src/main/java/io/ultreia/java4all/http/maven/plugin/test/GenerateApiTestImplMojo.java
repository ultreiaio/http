package io.ultreia.java4all.http.maven.plugin.test;

/*-
 * #%L
 * Http :: Maven Plugin
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.github.mustachejava.Mustache;
import io.ultreia.java4all.http.maven.plugin.HttpMojoSupport;
import io.ultreia.java4all.http.spi.model.ClassDescription;
import io.ultreia.java4all.http.spi.model.ImportManager;
import io.ultreia.java4all.http.spi.model.MethodDescription;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;

/**
 * To generated Impl Tests Support
 * Created on 01/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.26
 */
@Mojo(name = "generate-api-test-impl", threadSafe = true, defaultPhase = LifecyclePhase.GENERATE_SOURCES, requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME)
public class GenerateApiTestImplMojo extends HttpMojoSupport {

    /**
     * Where to generate classes.
     */
    @Parameter(defaultValue = "${project.build.directory}/generated-sources/http", required = true)
    private File outputDirectory;

    /**
     * Where are the sources (used to detect if we should generate concrete classes).
     */
    @Parameter(defaultValue = "${project.basedir}/src/main/java", required = true)
    private File sourcesDirectory;

    /**
     * Suffix to add to generated classes name.
     */
    @Parameter(required = true, defaultValue = "TestSupport")
    private String classSuffix;

    /**
     * Service support.
     */
    @Parameter(required = true)
    private String serviceSupportType;

    /**
     * Package name where to generate.
     */
    @Parameter(required = true)
    private String packageName;
    /**
     * Optional annotation to place for service method test on a {@code commit} on it.
     */
    @Parameter
    private String commitAnnotation;
    /**
     * Optional service provider name to use in generated files.
     */
    @Parameter(property = "http.serviceProviderName")
    private String serviceProviderName;
    /**
     * To generate missing concrete files in sources.
     */
    @Parameter(defaultValue = "false")
    private boolean generateConcreteToSource;

    private String serviceSupportTypeSimpleName;

    @Override
    protected void doAction() throws MojoExecutionException {
        Class<?> serviceTypeClass = getClass(serviceType);

        Class<?> commitAnnotation = null;
        if (this.commitAnnotation != null && !this.commitAnnotation.isBlank()) {
            commitAnnotation = getClass(this.commitAnnotation);
        }

        serviceSupportTypeSimpleName = serviceSupportType.substring(serviceSupportType.lastIndexOf(".") + 1);

        Path targetPath = getTargetPath(outputDirectory, packageName);
        Path sourcePath = getTargetPath(sourcesDirectory, packageName);

        getLog().debug(String.format("Will generate in: %s", targetPath));

        project.addCompileSourceRoot(outputDirectory.toString());

        Mustache mustache = getMustache(getClass(), "Generated");
        Mustache mustacheConcrete = getMustache(getClass(), "Concrete");

        List<Class<?>> services = getServices(serviceTypeClass);

        String servicePackageName = serviceTypeClass.getPackage().getName();

        for (Class<?> serviceType : services) {

            String serviceName = serviceType.getSimpleName();

            ClassDescription model = createModel(serviceTypeClass, serviceType, commitAnnotation);

            Path servicePath = getServicePath(targetPath, servicePackageName, serviceType);
            Path sourceServicePath = getServicePath(sourcePath, servicePackageName, serviceType);
            generate(servicePath, sourceServicePath, serviceName, mustache, mustacheConcrete, model.toReadModel(
                    serviceType.getPackage().getName() + "." + serviceName + "ReadTest",
                    serviceSupportType + "Read").orElse(null));
            generate(servicePath, sourceServicePath, serviceName, mustache, mustacheConcrete, model.toWriteModel(
                    serviceType.getPackage().getName() + "." + serviceName + "WriteTest",
                    serviceSupportType + "Write").orElse(null));
        }
    }

    protected void generate(Path servicePath, Path sourceServicePath, String serviceName, Mustache mustache, Mustache mustacheConcrete, ClassDescription model) throws MojoExecutionException {
        if (model == null) {
            return;
        }
        String testName = serviceName + model.getClassSuffix() + model.getReadOrWrite() + "Test.java";
        Path abstractPath = servicePath.resolve("Generated" + testName);
        if (isVerbose()) {
            getLog().info(String.format("Will generate for %s to %s", model.getServiceName(), abstractPath));
        }
        generate(mustache, abstractPath, model);
        Path concreteSourcePath = sourceServicePath.resolve(testName);
        if (Files.notExists(concreteSourcePath)) {
            Path concretePath = generateConcreteToSource ? concreteSourcePath : servicePath.resolve(testName);
            if (isVerbose()) {
                getLog().info(String.format("Will generate for %s to %s", model.getServiceName(), concretePath));
            }
            generate(mustacheConcrete, concretePath, toConcrete(model));
        }
    }


    ClassDescription toConcrete(ClassDescription model) {
        return new ClassDescription(
                model.getReadOrWrite(),
                model.getServiceProviderClass(),
                model.getPackageName(),
                model.getServiceName(),
                model.getClassSuffix(),
                model.getServiceSupportName(),
                model.getMethods(),
                new ImportManager().toDescription());
    }

    private <S> ClassDescription createModel(Class<?> contractType, Class<S> serviceType, Class<?> commitAnnotation) {

        String servicePackageName = serviceType.getPackage().getName();

        ImportManager importManager = new ImportManager();

        String commitAnnotationText = commitAnnotation == null ? "" : "\n    @" + commitAnnotation.getSimpleName();
        if (commitAnnotation != null) {
            importManager.importType(commitAnnotation);
        }
        if (serviceProviderName != null) {
            importManager.importType(serviceProviderName);
        }

        List<MethodDescription> methods = new LinkedList<>();
        MethodDescription.createMethodDescriptions(logInfo(),
                                                   contractType,
                                                   serviceType,
                                                   methods,
                                                   method -> new MethodDescriptionWitCommitAnnotation(method, commitAnnotationText));

        String serviceName = serviceType.getSimpleName();
        String servicePackage = getServicePackage(packageName, servicePackageName, contractType);

        importManager.importType(serviceType.getPackage().getName() + "." + serviceName);
//        importManager.importType(serviceType.getPackage().getName() + "." + serviceName + "Test");
        importManager.importType(serviceType.getPackage().getName() + "." + serviceName + "Fixtures");
//        importManager.importType(serviceSupportType);

        methods.sort(Comparator.comparing(MethodDescription::getName));
        return new ClassDescription(
                serviceProviderName,
                servicePackage,
                serviceName,
                classSuffix,
                serviceSupportTypeSimpleName,
                new LinkedHashSet<>(methods),
                importManager.toDescription());
    }
}
