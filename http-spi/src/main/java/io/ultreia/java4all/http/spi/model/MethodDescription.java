package io.ultreia.java4all.http.spi.model;

/*-
 * #%L
 * Http :: SPI
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by tchemit on 23/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface MethodDescription {

    static <S, M> void createMethodDescriptions(Consumer<String> log, Class<?> contractType, Class<S> serviceType, Collection<M> result, Function<Method, M> function) {
        for (Method declaredMethod : getDeclaredMethods(contractType, serviceType)) {
            if (log != null) {
                log.accept(String.format("Detect method %s", declaredMethod));
            }
            result.add(function.apply(declaredMethod));
        }
    }

    static <S> List<Method> getDeclaredMethods(Class<?> contractType, Class<S> serviceType) {
        List<Method> result = new LinkedList<>();
        getDeclaredMethods0(contractType, serviceType, result);
        return result;
    }

    static List<Class<?>> getExceptions(Method method, ImportManager importManager) {
        List<Class<?>> exceptions = new LinkedList<>();
        for (Class<?> aClass : method.getExceptionTypes()) {
            importManager.importType(aClass.getName());
            exceptions.add(aClass);
        }
        return exceptions;
    }

    private static <S> void getDeclaredMethods0(Class<?> contractType, Class<S> serviceType, List<Method> result) {
        Method[] declaredMethods = serviceType.getDeclaredMethods();
        for (Method declaredMethod : declaredMethods) {
            if (declaredMethod.isDefault()) {
                continue;
            }
            if (Modifier.isStatic(declaredMethod.getModifiers())) {
                continue;
            }
            result.add(declaredMethod);
        }
        for (Class<?> anInterface : serviceType.getInterfaces()) {
            if (contractType.isAssignableFrom(anInterface)) {
                getDeclaredMethods0(contractType, anInterface, result);
            }
        }
    }

    String getName();

    String getReturnType();

    String getParametersDefinition();

    List<Class<?>> getExceptions();

    boolean isWrite();

    default boolean isRead() {
        return !isWrite();
    }

    default boolean withExceptions() {
        return !getExceptions().isEmpty();
    }

    default String getExceptionsList() {
        return !getExceptions().isEmpty() ? (" throws " + Joiner.on(", ").join(getExceptions().stream().map(Class::getSimpleName).collect(Collectors.toList()))) : "";
    }
}
