package io.ultreia.java4all.http.spi.model;

/*-
 * #%L
 * Http :: SPI
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.TypeVariable;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Created by tchemit on 23/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ImportManager {

    private final Set<String> cache = new TreeSet<>();

    public String importType(Class<?> type) {

        if (type.getPackage() != null && !"void".equals(type.getSimpleName()) && !type.isPrimitive() && !type.getPackage().getName().equals("java.lang")) {

            cache.add(type.getName());
        }
        return type.getSimpleName();
    }

    public String getGenericDefinitionOfReturnType(Method method) {


        TypeVariable<?>[] typeparms = method.getTypeParameters();

        StringBuilder sb = new StringBuilder("");
        if (typeparms.length > 0) {

            for (TypeVariable<?> typeparm : typeparms) {

                sb.append(", ");
                sb.append(typeparm.getName());
                if (typeparm.getBounds().length > 0) {
                    sb.append(" extends ").append(importType(typeparm.getBounds()[0].getTypeName()));
                }
            }
            return String.format("<%s> ", sb.substring(2));
        }

        return "";
    }

    public Set<String> getGenericDefinitionsOfReturnType(Method method) {


        TypeVariable<?>[] typeparms = method.getTypeParameters();

        Set<String> result = new LinkedHashSet<>();
        if (typeparms.length > 0) {

            for (TypeVariable<?> typeparm : typeparms) {

                result.add(typeparm.getName());
            }
        }
        return result;
    }

    public String importReturnType(Method method) {
        return importReturnType(method, null);
    }

    public String importReturnType(Method method, Map<String, String> genericMapping) {
        return importComplexType(method.getGenericReturnType().getTypeName(), genericMapping);
    }

    public String importGenericParts(String genericParts) {
        StringBuilder result = new StringBuilder();
        Iterator<String> iterator = splitGenericParts(genericParts).iterator();
        while (iterator.hasNext()) {
            String genericPart = iterator.next();
            List<String> parts = splitType(genericPart);
            for (String part : parts) {
                result.append(part);
            }
            if (iterator.hasNext()) {
                result.append(", ");
            }
        }
        return result.toString();
    }

    public String importComplexType(String type, Map<String, String> genericMapping) {
        StringBuilder result = new StringBuilder();
        for (String part : splitType(type, genericMapping)) {
            result.append(part);
        }
        return result.toString();
    }

    private List<String> splitType(String typeName) {
        return splitType(typeName, null);
    }

    private List<String> splitType(String typeName, Map<String, String> genericMapping) {
        List<String> result = new LinkedList<>();
        if (!typeName.contains("<")) {
            // simple type
            result.add(importSimpleType(typeName, genericMapping));
            return result;
        }
        // Got a xxx<yyy>
        int firstGenericIndex = typeName.indexOf('<');
        int lastGenericIndex = typeName.lastIndexOf('>');
        String mainType = typeName.substring(0, firstGenericIndex);
        result.add(importSimpleType(mainType, genericMapping));
        result.add("<");
        String rest = typeName.substring(firstGenericIndex + 1, lastGenericIndex);
        List<String> genericParts = splitGenericParts(rest);
        Iterator<String> iterator = genericParts.iterator();
        while (iterator.hasNext()) {
            String genericPart = iterator.next();
            List<String> parts = splitType(genericPart, genericMapping);
            result.addAll(parts);
            if (iterator.hasNext()) {
                result.add(", ");
            }
        }
        result.add(">");
        return result;
    }

    private List<String> splitGenericParts(String typeName) {
        List<String> result = new LinkedList<>();
        int nbStart = 0;
        int firstIndex = 0;
        for (int i = 0, l = typeName.length(); i < l; i++) {
            char c = typeName.charAt(i);
            switch (c) {
                case '<':
                    nbStart++;
                    break;
                case '>':
                    nbStart--;
                    break;
                case ',':
                    if (nbStart == 0) {

                        // Got a safe ,
                        result.add(typeName.substring(firstIndex, i).trim());

                        firstIndex = i + 1;
                    }
            }

        }
        if (firstIndex < typeName.length()) {
            result.add(typeName.substring(firstIndex).trim());
        }
        return result;
    }

    public String importParameterType(Parameter parameter) {
        return importParameterType(parameter, null);
    }

    public String importParameterType(Parameter parameter, Map<String, String> genericMapping) {

        return importComplexType(parameter.getParameterizedType().getTypeName(), genericMapping);
    }

    public String importTypeAndRemoveGeneric(String type) {
        String result = importType(type);
        int endIndex = result.indexOf('<');
        if (endIndex > -1) {
            result = result.substring(0, endIndex);
        }
        return result;
    }

    public String removeGeneric(String type) {
        int endIndex = type.indexOf('<');
        return endIndex > -1 ? type.substring(0, endIndex) : type;
    }

    private String importSimpleType(String type, Map<String, String> genericMapping) {
        if (type.contains(" extends ")) {
            int i = type.indexOf(" extends ");
            return type.substring(0, i) + " extends " + importType(type.substring(i + 9));
        }
        if (type.startsWith("java.lang.")) {
            return getLast(type);
        }
        if (genericMapping != null && genericMapping.containsKey(type.trim())) {
            return genericMapping.get(type.trim());
        }
        return type;
    }

    public String importType(String type) {
        return importType(type, null);
    }

    public String importType(String type, Map<String, String> genericMapping) {

        type = type.trim();

        if (type.contains("<")) {

            // generic type
            int endIndex = type.indexOf('<');
            int lastEndIndex = type.lastIndexOf('>');
            String mainType = importType(type.substring(0, endIndex));
            String[] genericParts = type.substring(endIndex + 1, lastEndIndex).split(",");
            StringBuilder stringBuilder = new StringBuilder();
            for (String genericPart : genericParts) {
                stringBuilder.append(", ").append(importType(genericPart));
            }
            return String.format("%s<%s>", mainType, stringBuilder.substring(2));
        }

        if (type.contains(" extends ")) {
            int i = type.indexOf(" extends ");
            return type.substring(0, i) + " extends " + importType(type.substring(i + 9));
        }
        if (!"void".equals(type) && !type.startsWith("java.lang.") && type.contains(".")) {
            cache.add(type);
        }
        return getLast(type);
    }

    String getLast(String type) {
        String[] split = type.split("\\.");
        return split[split.length - 1];
    }

    public List<ImportDescription> toDescription() {
        List<String> list = new LinkedList<>(cache);
        list.sort(String::compareTo);
        return list.stream().map(ImportDescription::new).collect(Collectors.toList());
    }

}
