package io.ultreia.java4all.http.spi.model;

/*-
 * #%L
 * Http :: SPI
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by tchemit on 16/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ClassMappingTestDescription {

    private final String packageName;
    private final String classSuffix;
    private final String mojoName;
    private final List<ImportDescription> imports;
    private final Set<ServiceMapping> services;

    public ClassMappingTestDescription(String packageName, String classSuffix, String mojoName, List<ImportDescription> imports, Set<ServiceMapping> services) {
        this.packageName = packageName;
        this.classSuffix = classSuffix;
        this.mojoName = mojoName;
        this.imports = imports;
        this.services = services;
    }

    public Set<ServiceMapping> getServices() {
        return services;
    }

    public List<ImportDescription> getImports() {
        return imports;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getClassSuffix() {
        return classSuffix;
    }

    public String getMojoName() {
        return mojoName;
    }

    public Date getDate() {
        return Date.from(Instant.now());
    }
}
