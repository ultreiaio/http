package io.ultreia.java4all.http.spi.model;

/*-
 * #%L
 * Http :: SPI
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by tchemit on 23/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ClassDescription {

    private final String packageName;
    private final String classSuffix;
    private final String serviceName;
    private final String serviceSupportName;
    private final Set<MethodDescription> methods;
    private final List<ImportDescription> imports;
    private final ImmutableSet<Class<?>> exceptionTypes;
    private final String readOrWrite;
    private final String serviceProviderClass;

    public String getReadOrWrite() {
        return readOrWrite;
    }

    public ClassDescription(String serviceProviderClassName, String packageName, String serviceName, String classSuffix, String serviceSupportName, Set<MethodDescription> methods, List<ImportDescription> imports) {
        this("", serviceProviderClassName, packageName, serviceName, classSuffix, serviceSupportName, methods, imports);
    }

    public ClassDescription(String readOrWrite, String serviceProviderClassName, String packageName, String serviceName, String classSuffix, String serviceSupportName, Set<MethodDescription> methods, List<ImportDescription> imports) {
        this.readOrWrite = readOrWrite;
        this.serviceProviderClass = serviceProviderClassName;
        this.packageName = packageName;
        this.serviceName = serviceName;
        this.classSuffix = classSuffix;
        this.serviceSupportName = serviceSupportName;
        this.methods = methods;
        this.imports = imports;
        ImmutableSet.Builder<Class<?>> builder = ImmutableSet.builder();
        for (MethodDescription method : methods) {
            builder.addAll(method.getExceptions());
        }
        exceptionTypes = builder.build();
    }

    public Optional<ClassDescription> toReadModel(String... extraImports) {
        Set<MethodDescription> filterMethods = methods.stream().filter(MethodDescription::isRead).collect(Collectors.toSet());
        List<ImportDescription> newImports = new ArrayList<>(imports);
        for (String extraImport : extraImports) {
            newImports.add(new ImportDescription(extraImport));
        }
        return Optional.ofNullable(filterMethods.isEmpty() ? null : new ClassDescription("Read", serviceProviderClass, packageName, serviceName, classSuffix, serviceSupportName, filterMethods, newImports));
    }

    public Optional<ClassDescription> toWriteModel(String... extraImports) {
        Set<MethodDescription> filterMethods = methods.stream().filter(MethodDescription::isWrite).collect(Collectors.toSet());
        List<ImportDescription> newImports = new ArrayList<>(imports);
        for (String extraImport : extraImports) {
            newImports.add(new ImportDescription(extraImport));
        }
        return Optional.ofNullable(filterMethods.isEmpty() ? null : new ClassDescription("Write", serviceProviderClass, packageName, serviceName, classSuffix, serviceSupportName, filterMethods, newImports));
    }

    public String getServiceProviderClass() {
        return serviceProviderClass;
    }

    public String getServiceProviderInvocation() {
        return serviceProviderClass == null ? "" : ("servicesProvider, ");
    }

    public String getServiceProviderInvocationParameter() {
        return serviceProviderClass == null ? "" : ("getServicesProvider(), ");
    }

    public String getServiceProviderParameter() {
        return serviceProviderClass == null ? "" : (serviceProviderClass + " servicesProvider, ");
    }

    public String getClassSuffix() {
        return classSuffix;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public Set<MethodDescription> getMethods() {
        return methods;
    }

    public List<ImportDescription> getImports() {
        return imports;
    }

    public Date getDate() {
        return Date.from(Instant.now());
    }

    public String getServiceSupportName() {
        return serviceSupportName;
    }

    public Set<Class<?>> getExceptionTypes() {
        return exceptionTypes;
    }

    public boolean withExceptionTypes() {
        return !exceptionTypes.isEmpty();
    }
}
