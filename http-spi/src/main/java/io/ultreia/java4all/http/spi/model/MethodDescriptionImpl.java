package io.ultreia.java4all.http.spi.model;

/*-
 * #%L
 * Http :: SPI
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.http.spi.Nullable;
import io.ultreia.java4all.http.spi.SpiHelper;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created on 03/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.28
 */
public class MethodDescriptionImpl implements MethodDescription {

    private final String name;
    private final String returnType;
    private final String parametersDefinition;
    private final String parametersInvocation;
    private final String returnInvocation;
    private final String checkParameters;
    private final List<Class<?>> exceptions;
    private final List<java.lang.reflect.Parameter> parameters;
    private final boolean write;

    public MethodDescriptionImpl(String declaringClass, ImportManager importManager, Method method, Map<String, String> currentMapping) {
        this.name = method.getName();
        String returnTypePrefix = importManager.getGenericDefinitionOfReturnType(method);
        String simpleReturnType = importManager.importReturnType(method, currentMapping);
        this.returnType = returnTypePrefix + simpleReturnType;
        this.parameters = Arrays.asList(method.getParameters());
        write = SpiHelper.write(method);
        StringBuilder parametersInvocationBuilder = new StringBuilder();
        StringBuilder parametersDefinitionBuilder = new StringBuilder();
        StringBuilder checkParametersBuilder = new StringBuilder();
        for (java.lang.reflect.Parameter parameter : method.getParameters()) {
            String name = parameter.getName();
            boolean nullable = SpiHelper.nullable(parameter);
            parametersDefinitionBuilder.append(", ");
            if (nullable) {
                importManager.importType(Nullable.class);
                parametersDefinitionBuilder.append("@").append(Nullable.class.getSimpleName()).append(" ");
            } else if (!parameter.getType().isPrimitive()) {
                importManager.importType(Objects.class);
                checkParametersBuilder.append(String.format("        Objects.requireNonNull(%1$s, \"Parameter '%1$s' (in method %2$s#%3$s) can not be null.\");\n", name, declaringClass, this.name));
            }
            parametersInvocationBuilder.append(", ").append(name);
            parametersDefinitionBuilder.append(importManager.importParameterType(parameter, currentMapping)).append(" ").append(name);
        }

        String parametersInvocation = "";
        if (parametersInvocationBuilder.length() > 0) {
            parametersInvocation = parametersInvocationBuilder.substring(2);
        }
        this.parametersInvocation = parametersInvocation;

        String parametersDefinition = "";
        if (parametersDefinitionBuilder.length() > 0) {
            parametersDefinition = parametersDefinitionBuilder.substring(2);
        }
        this.parametersDefinition = parametersDefinition;
        this.checkParameters = checkParametersBuilder.toString();
        this.returnInvocation = returnType.contains("void") ? "" : "return ";
        this.exceptions = MethodDescription.getExceptions(method, importManager);
    }

    @Override
    public boolean isWrite() {
        return write;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getReturnType() {
        return returnType;
    }

    @Override
    public String getParametersDefinition() {
        return parametersDefinition;
    }

    @Override
    public List<Class<?>> getExceptions() {
        return exceptions;
    }

    @SuppressWarnings("unused")
    public String getParametersInvocation() {
        return parametersInvocation;
    }

    @SuppressWarnings("unused")
    public String getReturnInvocation() {
        return returnInvocation;
    }

    public List<java.lang.reflect.Parameter> getParameters() {
        return parameters;
    }

    public String getCheckParameters() {
        return checkParameters;
    }
}
