package io.ultreia.java4all.http.spi.model;

/*-
 * #%L
 * Http :: SPI
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.http.spi.Internal;
import org.reflections.Reflections;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class ServiceMapping {

    private final Class<?> serviceType;
    private final String serviceImplementation;
    private final boolean anonymous;
    public static void loadGenericInterfacesWithGenerics(List<ParameterizedType> genericInterfacesWithGenerics, Class<?> contractType, Class<?> typeToScan) {
        Type[] genericInterfacesArray = typeToScan.getGenericInterfaces();
        for (Type type : genericInterfacesArray) {
            if (contractType.equals(type)) {
                continue;
            }
            if (type instanceof ParameterizedType) {
                genericInterfacesWithGenerics.add((ParameterizedType) type);
                Type rawType = ((ParameterizedType) type).getRawType();
                if (rawType instanceof Class) {
                    loadGenericInterfacesWithGenerics(genericInterfacesWithGenerics, contractType, (Class<?>) rawType);
                }
            } else {
                loadGenericInterfacesWithGenerics(genericInterfacesWithGenerics, contractType, (Class<?>) type);
            }
        }
    }

    public static <S> Map<String, String> genericMapping(Consumer<String> log, Class<?> contractType, Class<S> serviceType) {
        List<ParameterizedType> genericInterfacesWithGenerics = new LinkedList<>();
        loadGenericInterfacesWithGenerics(genericInterfacesWithGenerics, contractType, serviceType);
        Map<String, String> result = new LinkedHashMap<>();
        for (ParameterizedType type : genericInterfacesWithGenerics) {
            log.accept("Use generic type: " + type);
            Type[] actualTypeArguments = type.getActualTypeArguments();
            Class<?> rawType = (Class<?>) type.getRawType();
            TypeVariable<? extends Class<?>>[] typeParameters = rawType.getTypeParameters();
            for (int i = 0; i < typeParameters.length; i++) {
                TypeVariable<? extends Class<?>> typeParameter = typeParameters[i];
                Type actualTypeParameter = actualTypeArguments[i];
                if (!Objects.equals(typeParameter.getTypeName(), actualTypeParameter.getTypeName())) {
                    result.put(typeParameter.getTypeName(), actualTypeParameter.getTypeName());
                    log.accept(String.format("Register generic type on %s: %s → %s", rawType.getName(), typeParameter, actualTypeParameter));
                }
            }
        }
        return result;
    }

    public static List<Class<?>> getAllServices(Class<?> serviceTypeClass) {
        return new Reflections(serviceTypeClass.getPackage().getName()).getSubTypesOf(serviceTypeClass).stream().filter(Class::isInterface).filter(c -> !c.isAnnotationPresent(Internal.class)).sorted(Comparator.comparing(Class::getName)).collect(Collectors.toList());
    }

    public ServiceMapping(Class<?> serviceType, String serviceImplementation, boolean anonymous) {
        this.serviceType = serviceType;
        this.serviceImplementation = serviceImplementation;
        this.anonymous = anonymous;
    }

    public Class<?> getServiceType() {
        return serviceType;
    }

    public String getServiceImplementation() {
        return serviceImplementation;
    }

    public boolean isAnonymous() {
        return anonymous;
    }
}
