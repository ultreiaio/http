package io.ultreia.java4all.http.spi.model;

/*-
 * #%L
 * Http :: SPI
 * %%
 * Copyright (C) 2017 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.SetMultimap;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Map;

/**
 * Created by tchemit on 26/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ImportManagerTest {

    private ImportManager importManager;

    @Before
    public void setUp() {
        importManager = new ImportManager();
    }

    public SetMultimap<Class<? extends Serializable>, String> getReferentialIds() {
        return null;
    }

    public <O extends Serializable> SetMultimap<Class<O>, String> getReferentialIds2() {
        return null;
    }

    public <D extends Serializable, R extends Collection<D>> Map<D, R> getReferenceSet() {
        return null;
    }

    public ImmutableSet<Map<?, ?>> getReferentialReferenceSets() {
        return null;
    }

    @Test
    public void importType() throws Exception {
        assertReturnType(getClass().getMethod("getReferentialIds"), "com.google.common.collect.SetMultimap<Class<? extends Serializable>, String>");
        assertReturnType(getClass().getMethod("getReferentialIds2"), "com.google.common.collect.SetMultimap<Class<O>, String>");
        assertReturnType(getClass().getMethod("getReferenceSet"), "java.util.Map<D, R>");
        assertReturnType(getClass().getMethod("getReferentialReferenceSets"), "com.google.common.collect.ImmutableSet<java.util.Map<?, ?>>");
    }

    private void assertReturnType(Method method, String expectedReturnType) {
        String returnType = importManager.importReturnType(method);
        Assert.assertEquals(expectedReturnType, returnType);
    }

}
