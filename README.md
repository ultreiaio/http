[![Maven Central status](https://img.shields.io/maven-central/v/io.ultreia.java4all/http.svg)](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22io.ultreia.java4all%22%20AND%20a%3A%22http%22)
[![build status](https://gitlab.com/ultreiaio/http/badges/develop/pipeline.svg)](https://gitlab.com/ultreiaio/http/commits/develop)
[![The GNU Lesser General Public License, Version 3.0](https://img.shields.io/badge/license-LGPL3-grren.svg)](http://www.gnu.org/licenses/lgpl-3.0.txt)

# Http API project

TODO


# Resources

* [Changelog and downloads](https://gitlab.com/ultreiaio/http/blob/develop/CHANGELOG.md)
* [Documentation](https://ultreiaio.gitlab.io/http)

# Community

* [Contact](mailto:dev@tchemit.fr)
