# http changelog

 * Author [Tony Chemit](mailto:dev@tchemit.fr)
 * Last generated at 2025-02-02 14:26.

## Version [2.0.7](https://gitlab.com/ultreiaio/http/-/milestones/56)

**Closed at 2025-02-02.**


### Issues
  * [[enhancement 67]](https://gitlab.com/ultreiaio/http/-/issues/67) **Remove GenerateServicesProviderMojo mojo (now use the annotation processor io.ultreia.java4all.http.spi.processor.ServiceProcessor instead)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 68]](https://gitlab.com/ultreiaio/http/-/issues/68) **in any mojo, be able to use module dependencies instead of adding them to the plugin invocation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 69]](https://gitlab.com/ultreiaio/http/-/issues/69) **Remove io.ultreia.java4all.http.spi.method.ServiceMethodNameTranslator API (keep the only the package implementation used by the processor io.ultreia.java4all.http.spi.processor.ServiceProcessor)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 70]](https://gitlab.com/ultreiaio/http/-/issues/70) **Do not used dependencies version from parent pom properties and clean project pom** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.0.6](https://gitlab.com/ultreiaio/http/-/milestones/55)

**Closed at 2024-04-25.**


### Issues
  * [[bug 66]](https://gitlab.com/ultreiaio/http/-/issues/66) **Method DELETE has no body, always use url encode** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.0.5](https://gitlab.com/ultreiaio/http/-/milestones/54)

**Closed at 2024-03-26.**


### Issues
  * [[enhancement 65]](https://gitlab.com/ultreiaio/http/-/issues/65) **Replace addAuthenticationToken annotation method on requests and move at service level (via a anonymous method of Service annotation)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.0.4](https://gitlab.com/ultreiaio/http/-/milestones/53)

**Closed at 2024-03-21.**


### Issues
  * [[enhancement 64]](https://gitlab.com/ultreiaio/http/-/issues/64) **Review how to deal with nullable parameters** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.0.3](https://gitlab.com/ultreiaio/http/-/milestones/52)

**Closed at 2024-03-20.**


### Issues
  * [[enhancement 62]](https://gitlab.com/ultreiaio/http/-/issues/62) **Improve CheckServicesTypesMojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 63]](https://gitlab.com/ultreiaio/http/-/issues/63) **Always use json to serialize iterables and maps in GenerateHttpClientMojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.0.2](https://gitlab.com/ultreiaio/http/-/milestones/51)

**Closed at 2024-01-24.**


### Issues
  * [[enhancement 61]](https://gitlab.com/ultreiaio/http/-/issues/61) **Add a new Mojo to check services used types (to avoid server deserialization problems)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.0.1](https://gitlab.com/ultreiaio/http/-/milestones/50)

**Closed at 2023-11-25.**


### Issues
  * [[enhancement 60]](https://gitlab.com/ultreiaio/http/-/issues/60) **Sanitize HRequest and HResponse** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.47](https://gitlab.com/ultreiaio/http/-/milestones/49)

**Closed at 2023-07-12.**


### Issues
  * [[bug 58]](https://gitlab.com/ultreiaio/http/-/issues/58) **Fix NPE when exception is not well deserialize in service client** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.45](https://gitlab.com/ultreiaio/http/-/milestones/47)

**Closed at 2022-07-31.**


### Issues
  * [[bug 54]](https://gitlab.com/ultreiaio/http/-/issues/54) **Improve generated service client to have TimeLog at nice place** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 54]](https://gitlab.com/ultreiaio/http/-/issues/54) **Improve generated service client to have TimeLog at nice place** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 55]](https://gitlab.com/ultreiaio/http/-/issues/55) **Replace url by uri in HResponseNotAvailableException to avoid nasty IOException** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.44](https://gitlab.com/ultreiaio/http/-/milestones/46)

**Closed at 2022-07-30.**


### Issues
No issue.

## Version [1.0.43](https://gitlab.com/ultreiaio/http/-/milestones/45)

**Closed at 2022-06-27.**


### Issues
  * [[bug 53]](https://gitlab.com/ultreiaio/http/-/issues/53) **Runtime exceptions are not well catched in generated service client** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.42](https://gitlab.com/ultreiaio/http/-/milestones/43)

**Closed at 2022-06-20.**


### Issues
  * [[enhancement 52]](https://gitlab.com/ultreiaio/http/-/issues/52) **Improve HResponseErrorAdapter to be able to fix gson deserialisation for Exception with java 17** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.41](https://gitlab.com/ultreiaio/http/-/milestones/42)

**Closed at 2022-03-20.**


### Issues
  * [[enhancement 49]](https://gitlab.com/ultreiaio/http/-/issues/49) **Remove JSoup usage in http-api** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 50]](https://gitlab.com/ultreiaio/http/-/issues/50) **Remove guava usage in HResponse API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.40](https://gitlab.com/ultreiaio/http/-/milestones/41)

**Closed at 2022-03-04.**


### Issues
  * [[enhancement 48]](https://gitlab.com/ultreiaio/http/-/issues/48) **Improve Json API (add default adapters)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.39](https://gitlab.com/ultreiaio/http/-/milestones/40)

**Closed at 2022-02-13.**


### Issues
  * [[enhancement 47]](https://gitlab.com/ultreiaio/http/-/issues/47) **Add thin API to collect Gson adapters** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.38](https://gitlab.com/ultreiaio/http/-/milestones/39)

**Closed at 2022-02-09.**


### Issues
  * [[enhancement 43]](https://gitlab.com/ultreiaio/http/-/issues/43) **Improve generate-services-provider mojo (add optional supper-class)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 44]](https://gitlab.com/ultreiaio/http/-/issues/44) **Introduce thin API for Services** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 45]](https://gitlab.com/ultreiaio/http/-/issues/45) **Use ImmutableClassMappingInstance in generated classMapping for services :)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 46]](https://gitlab.com/ultreiaio/http/-/issues/46) **Introduce Service annotation to generate ServicesProvider** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.37](https://gitlab.com/ultreiaio/http/-/milestones/38)

**Closed at 2021-11-18.**


### Issues
  * [[enhancement 42]](https://gitlab.com/ultreiaio/http/-/issues/42) **Split generated tests in two : Read or Write** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.37](https://gitlab.com/ultreiaio/http/-/milestones/38)

**Closed at 2021-11-18.**


### Issues
  * [[enhancement 42]](https://gitlab.com/ultreiaio/http/-/issues/42) **Split generated tests in two : Read or Write** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.36](https://gitlab.com/ultreiaio/http/-/milestones/37)

**Closed at 2021-10-12.**


### Issues
  * [[enhancement 41]](https://gitlab.com/ultreiaio/http/-/issues/41) **Do not detect static methods and improve the way to collect MethodDescription** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.36](https://gitlab.com/ultreiaio/http/-/milestones/37)

**Closed at 2021-10-12.**


### Issues
  * [[enhancement 41]](https://gitlab.com/ultreiaio/http/-/issues/41) **Do not detect static methods and improve the way to collect MethodDescription** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.35](https://gitlab.com/ultreiaio/http/-/milestones/36)

**Closed at 2021-09-28.**


### Issues
  * [[bug 40]](https://gitlab.com/ultreiaio/http/-/issues/40) **generate-api still generate java files even if they exists on source** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.35](https://gitlab.com/ultreiaio/http/-/milestones/36)

**Closed at 2021-09-28.**


### Issues
  * [[bug 40]](https://gitlab.com/ultreiaio/http/-/issues/40) **generate-api still generate java files even if they exists on source** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.34](https://gitlab.com/ultreiaio/http/-/milestones/35)

**Closed at 2021-07-25.**


### Issues
  * [[enhancement 39]](https://gitlab.com/ultreiaio/http/-/issues/39) **Inject ServicesProvider inside fixtures and generated tests** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.34](https://gitlab.com/ultreiaio/http/-/milestones/35)

**Closed at 2021-07-25.**


### Issues
  * [[enhancement 39]](https://gitlab.com/ultreiaio/http/-/issues/39) **Inject ServicesProvider inside fixtures and generated tests** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.33](https://gitlab.com/ultreiaio/http/-/milestones/34)

**Closed at 2021-07-09.**


### Issues
  * [[enhancement 38]](https://gitlab.com/ultreiaio/http/-/issues/38) **Improve GenerateHttpClientMojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.33](https://gitlab.com/ultreiaio/http/-/milestones/34)

**Closed at 2021-07-09.**


### Issues
  * [[enhancement 38]](https://gitlab.com/ultreiaio/http/-/issues/38) **Improve GenerateHttpClientMojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.32](https://gitlab.com/ultreiaio/http/-/milestones/33)

**Closed at 2021-07-06.**


### Issues
  * [[enhancement 37]](https://gitlab.com/ultreiaio/http/-/issues/37) **Improve error return on 4xxx codes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.32](https://gitlab.com/ultreiaio/http/-/milestones/33)

**Closed at 2021-07-06.**


### Issues
  * [[enhancement 37]](https://gitlab.com/ultreiaio/http/-/issues/37) **Improve error return on 4xxx codes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.31](https://gitlab.com/ultreiaio/http/-/milestones/32)

**Closed at 2021-04-22.**


### Issues
  * [[enhancement 35]](https://gitlab.com/ultreiaio/http/-/issues/35) **Be able to skip prefixPath and package prefix in generated mapping** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.31](https://gitlab.com/ultreiaio/http/-/milestones/32)

**Closed at 2021-04-22.**


### Issues
  * [[enhancement 35]](https://gitlab.com/ultreiaio/http/-/issues/35) **Be able to skip prefixPath and package prefix in generated mapping** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.30](https://gitlab.com/ultreiaio/http/-/milestones/31)

**Closed at 2021-02-17.**


### Issues
  * [[enhancement 33]](https://gitlab.com/ultreiaio/http/-/issues/33) **Fixtures should be static in generated test impl** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 34]](https://gitlab.com/ultreiaio/http/-/issues/34) **Fix bad generated fixtures (string.format miss a parameter)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.30](https://gitlab.com/ultreiaio/http/-/milestones/31)

**Closed at 2021-02-17.**


### Issues
  * [[enhancement 33]](https://gitlab.com/ultreiaio/http/-/issues/33) **Fixtures should be static in generated test impl** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 34]](https://gitlab.com/ultreiaio/http/-/issues/34) **Fix bad generated fixtures (string.format miss a parameter)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.29](https://gitlab.com/ultreiaio/http/-/milestones/30)

**Closed at 2021-02-16.**


### Issues
  * [[enhancement 31]](https://gitlab.com/ultreiaio/http/-/issues/31) **Make Test impl just empty (no ignore, nor methods)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 32]](https://gitlab.com/ultreiaio/http/-/issues/32) **Improve generated concrete fixture** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.29](https://gitlab.com/ultreiaio/http/-/milestones/30)

**Closed at 2021-02-16.**


### Issues
  * [[enhancement 31]](https://gitlab.com/ultreiaio/http/-/issues/31) **Make Test impl just empty (no ignore, nor methods)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 32]](https://gitlab.com/ultreiaio/http/-/issues/32) **Improve generated concrete fixture** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.28](https://gitlab.com/ultreiaio/http/-/milestones/29)

**Closed at 2021-02-03.**


### Issues
  * [[enhancement 29]](https://gitlab.com/ultreiaio/http/-/issues/29) **Improve Tests mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 30]](https://gitlab.com/ultreiaio/http/-/issues/30) **Introduce annotation Write** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.28](https://gitlab.com/ultreiaio/http/-/milestones/29)

**Closed at 2021-02-03.**


### Issues
  * [[enhancement 29]](https://gitlab.com/ultreiaio/http/-/issues/29) **Improve Tests mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 30]](https://gitlab.com/ultreiaio/http/-/issues/30) **Introduce annotation Write** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.27](https://gitlab.com/ultreiaio/http/-/milestones/28)

**Closed at 2021-02-03.**


### Issues
  * [[enhancement 27]](https://gitlab.com/ultreiaio/http/-/issues/27) **Improve GenerateApiTestFixturesMojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 28]](https://gitlab.com/ultreiaio/http/-/issues/28) **Improve GenerateApiTestImplMojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.27](https://gitlab.com/ultreiaio/http/-/milestones/28)

**Closed at 2021-02-03.**


### Issues
  * [[enhancement 27]](https://gitlab.com/ultreiaio/http/-/issues/27) **Improve GenerateApiTestFixturesMojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 28]](https://gitlab.com/ultreiaio/http/-/issues/28) **Improve GenerateApiTestImplMojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.26](https://gitlab.com/ultreiaio/http/-/milestones/27)

**Closed at 2021-02-01.**


### Issues
  * [[enhancement 23]](https://gitlab.com/ultreiaio/http/-/issues/23) **Generate service api test** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 24]](https://gitlab.com/ultreiaio/http/-/issues/24) **Use java 11** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 25]](https://gitlab.com/ultreiaio/http/-/issues/25) **Generate service impl test** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 26]](https://gitlab.com/ultreiaio/http/-/issues/26) **Generate service test fixtures** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.26](https://gitlab.com/ultreiaio/http/-/milestones/27)

**Closed at 2021-02-01.**


### Issues
  * [[enhancement 23]](https://gitlab.com/ultreiaio/http/-/issues/23) **Generate service api test** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 24]](https://gitlab.com/ultreiaio/http/-/issues/24) **Use java 11** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 25]](https://gitlab.com/ultreiaio/http/-/issues/25) **Generate service impl test** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 26]](https://gitlab.com/ultreiaio/http/-/issues/26) **Generate service test fixtures** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.25](https://gitlab.com/ultreiaio/http/-/milestones/26)

**Closed at 2020-12-31.**


### Issues
  * [[bug 22]](https://gitlab.com/ultreiaio/http/-/issues/22) **Got a NPE each time an error occurs on http server** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.25](https://gitlab.com/ultreiaio/http/-/milestones/26)

**Closed at 2020-12-31.**


### Issues
  * [[bug 22]](https://gitlab.com/ultreiaio/http/-/issues/22) **Got a NPE each time an error occurs on http server** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.24](https://gitlab.com/ultreiaio/http/-/milestones/25)

**Closed at 2020-12-26.**


### Issues
  * [[enhancement 21]](https://gitlab.com/ultreiaio/http/-/issues/21) **Open API for external usages** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.24](https://gitlab.com/ultreiaio/http/-/milestones/25)

**Closed at 2020-12-26.**


### Issues
  * [[enhancement 21]](https://gitlab.com/ultreiaio/http/-/issues/21) **Open API for external usages** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.23](https://gitlab.com/ultreiaio/http/-/milestones/24)

**Closed at 2020-10-14.**


### Issues
  * [[enhancement 20]](https://gitlab.com/ultreiaio/http/-/issues/20) **Improve generics when using service with parent** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.23](https://gitlab.com/ultreiaio/http/-/milestones/24)

**Closed at 2020-10-14.**


### Issues
  * [[enhancement 20]](https://gitlab.com/ultreiaio/http/-/issues/20) **Improve generics when using service with parent** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.22](https://gitlab.com/ultreiaio/http/-/milestones/23)

**Closed at 2020-08-31.**


### Issues
  * [[bug 19]](https://gitlab.com/ultreiaio/http/-/issues/19) **Bad generated client java files (return type are not correct)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.22](https://gitlab.com/ultreiaio/http/-/milestones/23)

**Closed at 2020-08-31.**


### Issues
  * [[bug 19]](https://gitlab.com/ultreiaio/http/-/issues/19) **Bad generated client java files (return type are not correct)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.21](https://gitlab.com/ultreiaio/http/-/milestones/22)

**Closed at 2020-08-30.**


### Issues
  * [[enhancement 17]](https://gitlab.com/ultreiaio/http/-/issues/17) **Introduce @Internal annotation to get inheritance possible** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 18]](https://gitlab.com/ultreiaio/http/-/issues/18) **Improve generated service api java files** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.21](https://gitlab.com/ultreiaio/http/-/milestones/22)

**Closed at 2020-08-30.**


### Issues
  * [[enhancement 17]](https://gitlab.com/ultreiaio/http/-/issues/17) **Introduce @Internal annotation to get inheritance possible** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 18]](https://gitlab.com/ultreiaio/http/-/issues/18) **Improve generated service api java files** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.20](https://gitlab.com/ultreiaio/http/-/milestones/21)

**Closed at 2020-08-28.**


### Issues
  * [[enhancement 16]](https://gitlab.com/ultreiaio/http/-/issues/16) **Make HttpClient accept synonyms in service methods** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.20](https://gitlab.com/ultreiaio/http/-/milestones/21)

**Closed at 2020-08-28.**


### Issues
  * [[enhancement 16]](https://gitlab.com/ultreiaio/http/-/issues/16) **Make HttpClient accept synonyms in service methods** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.19](https://gitlab.com/ultreiaio/http/-/milestones/20)

**Closed at 2020-02-27.**


### Issues
  * [[enhancement 14]](https://gitlab.com/ultreiaio/http/-/issues/14) **Go back to java 8** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 15]](https://gitlab.com/ultreiaio/http/-/issues/15) **Improve HttpResponse code** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.19](https://gitlab.com/ultreiaio/http/-/milestones/20)

**Closed at 2020-02-27.**


### Issues
  * [[enhancement 14]](https://gitlab.com/ultreiaio/http/-/issues/14) **Go back to java 8** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 15]](https://gitlab.com/ultreiaio/http/-/issues/15) **Improve HttpResponse code** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.18](https://gitlab.com/ultreiaio/http/-/milestones/19)

**Closed at 2020-02-24.**


### Issues
  * [[bug 12]](https://gitlab.com/ultreiaio/http/-/issues/12) **Bad generation of client** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 13]](https://gitlab.com/ultreiaio/http/-/issues/13) **Migrates to java 10** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.18](https://gitlab.com/ultreiaio/http/-/milestones/19)

**Closed at 2020-02-24.**


### Issues
  * [[bug 12]](https://gitlab.com/ultreiaio/http/-/issues/12) **Bad generation of client** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 13]](https://gitlab.com/ultreiaio/http/-/issues/13) **Migrates to java 10** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.17](https://gitlab.com/ultreiaio/http/-/milestones/18)

**Closed at 2019-07-31.**


### Issues
  * [[enhancement 11]](https://gitlab.com/ultreiaio/http/-/issues/11) **Make possible to use synonyms in service names** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.17](https://gitlab.com/ultreiaio/http/-/milestones/18)

**Closed at 2019-07-31.**


### Issues
  * [[enhancement 11]](https://gitlab.com/ultreiaio/http/-/issues/11) **Make possible to use synonyms in service names** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.16](https://gitlab.com/ultreiaio/http/-/milestones/17)

**Closed at 2019-07-29.**


### Issues
  * [[enhancement 9]](https://gitlab.com/ultreiaio/http/-/issues/9) **Improve service provider generation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 10]](https://gitlab.com/ultreiaio/http/-/issues/10) **Add a way to translate service method name in service provider generator (to accept synonyms in service names)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.16](https://gitlab.com/ultreiaio/http/-/milestones/17)

**Closed at 2019-07-29.**


### Issues
  * [[enhancement 9]](https://gitlab.com/ultreiaio/http/-/issues/9) **Improve service provider generation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 10]](https://gitlab.com/ultreiaio/http/-/issues/10) **Add a way to translate service method name in service provider generator (to accept synonyms in service names)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.15](https://gitlab.com/ultreiaio/http/-/milestones/16)

**Closed at 2019-02-13.**


### Issues
  * [[enhancement 8]](https://gitlab.com/ultreiaio/http/-/issues/8) **Introduce GenerateServicesProviderMojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.15](https://gitlab.com/ultreiaio/http/-/milestones/16)

**Closed at 2019-02-13.**


### Issues
  * [[enhancement 8]](https://gitlab.com/ultreiaio/http/-/issues/8) **Introduce GenerateServicesProviderMojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.14](https://gitlab.com/ultreiaio/http/-/milestones/15)

**Closed at 2019-02-08.**


### Issues
  * [[bug 6]](https://gitlab.com/ultreiaio/http/-/issues/6) **Fix some generic returns while generating client API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 7]](https://gitlab.com/ultreiaio/http/-/issues/7) **Add a verbose mode in mojos** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.14](https://gitlab.com/ultreiaio/http/-/milestones/15)

**Closed at 2019-02-08.**


### Issues
  * [[bug 6]](https://gitlab.com/ultreiaio/http/-/issues/6) **Fix some generic returns while generating client API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 7]](https://gitlab.com/ultreiaio/http/-/issues/7) **Add a verbose mode in mojos** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.13](https://gitlab.com/ultreiaio/http/-/milestones/14)

**Closed at 2018-09-18.**


### Issues
  * [[bug 5]](https://gitlab.com/ultreiaio/http/-/issues/5) **Files are generated even if they already exists in source path** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.13](https://gitlab.com/ultreiaio/http/-/milestones/14)

**Closed at 2018-09-18.**


### Issues
  * [[bug 5]](https://gitlab.com/ultreiaio/http/-/issues/5) **Files are generated even if they already exists in source path** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.12](https://gitlab.com/ultreiaio/http/-/milestones/13)

**Closed at 2018-09-17.**


### Issues
  * [[enhancement 4]](https://gitlab.com/ultreiaio/http/-/issues/4) **Add socketTimeout in HRequest config** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.12](https://gitlab.com/ultreiaio/http/-/milestones/13)

**Closed at 2018-09-17.**


### Issues
  * [[enhancement 4]](https://gitlab.com/ultreiaio/http/-/issues/4) **Add socketTimeout in HRequest config** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.11](https://gitlab.com/ultreiaio/http/-/milestones/12)

**Closed at 2018-08-02.**


### Issues
  * [[enhancement 3]](https://gitlab.com/ultreiaio/http/-/issues/3) **Improve Log4J configuration** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.11](https://gitlab.com/ultreiaio/http/-/milestones/12)

**Closed at 2018-08-02.**


### Issues
  * [[enhancement 3]](https://gitlab.com/ultreiaio/http/-/issues/3) **Improve Log4J configuration** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.10](https://gitlab.com/ultreiaio/http/-/milestones/11)

**Closed at 2018-08-02.**


### Issues
  * [[enhancement 2]](https://gitlab.com/ultreiaio/http/-/issues/2) **Migrates to Log4J2** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.10](https://gitlab.com/ultreiaio/http/-/milestones/11)

**Closed at 2018-08-02.**


### Issues
  * [[enhancement 2]](https://gitlab.com/ultreiaio/http/-/issues/2) **Migrates to Log4J2** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.9](https://gitlab.com/ultreiaio/http/-/milestones/10)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0.9](https://gitlab.com/ultreiaio/http/-/milestones/10)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0.8](https://gitlab.com/ultreiaio/http/-/milestones/9)

**Closed at *In progress*.**


### Issues
  * [[enhancement 1]](https://gitlab.com/ultreiaio/http/-/issues/1) **Do not generate default methods** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.8](https://gitlab.com/ultreiaio/http/-/milestones/9)

**Closed at *In progress*.**


### Issues
  * [[enhancement 1]](https://gitlab.com/ultreiaio/http/-/issues/1) **Do not generate default methods** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.7](https://gitlab.com/ultreiaio/http/-/milestones/8)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0.7](https://gitlab.com/ultreiaio/http/-/milestones/8)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0.6](https://gitlab.com/ultreiaio/http/-/milestones/7)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0.6](https://gitlab.com/ultreiaio/http/-/milestones/7)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0.5](https://gitlab.com/ultreiaio/http/-/milestones/6)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0.5](https://gitlab.com/ultreiaio/http/-/milestones/6)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0.4](https://gitlab.com/ultreiaio/http/-/milestones/5)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0.4](https://gitlab.com/ultreiaio/http/-/milestones/5)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0.3](https://gitlab.com/ultreiaio/http/-/milestones/4)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0.3](https://gitlab.com/ultreiaio/http/-/milestones/4)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0.2](https://gitlab.com/ultreiaio/http/-/milestones/3)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0.2](https://gitlab.com/ultreiaio/http/-/milestones/3)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0.1](https://gitlab.com/ultreiaio/http/-/milestones/2)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0.1](https://gitlab.com/ultreiaio/http/-/milestones/2)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0.0](https://gitlab.com/ultreiaio/http/-/milestones/1)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0.0](https://gitlab.com/ultreiaio/http/-/milestones/1)

**Closed at *In progress*.**


### Issues
No issue.

